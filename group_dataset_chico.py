# Este archivo contiene algoritmos de agrupamiento, Feature Selection y mas aplicados sobre un dataset chico de 16 genes
# y unos pocos pacientes. Esto es util para corroborar la completitud del Blind Search y probar optimizaciones
import pandas as pd
from typing import Optional
from datasets import get_small_dataset
from metaheuristics import binary_black_hole
from optimizations import OptimizationTypes, get_optimization
from util import compute_survival_curve, blind_search, get_groups, \
    get_trainning_test_validation_split, group_using_k_means, get_logrank_info, is_normally_distributed, \
    get_most_related_pairs, filter_by_corr, get_columns_by_categorical, get_columns_from_df
import numpy as np

# Maxima cantidad de elementos a combinar, si se pone en None se realizan todas las combinaciones posibles
MAX_N_COMBINATIONS: Optional[int] = None

# Tipo de optimizacion a utilizar
OPTIMIZATION_METHOD: OptimizationTypes = 'logrank_p-value'

# Porcentaje del dataset para ser utilizado como Testing (valores entre 0.0 y 1.0). Si es None se usa un 0.25
TEST_SIZE = None

# Seteamos algunos datos del dataset para manejar la informacion
TIME_FIELD = 'OVERALL_SURVIVAL'
EVENT_FIELD = 'overall_survival_indicator'
EVENT_LIVING_VALUE = 0
EVEN_DECEASED_VALUE = 1


def compute_log_rank(index_array: np.array, df: pd.DataFrame, plot: bool = False):
    genes_to_fit = get_columns_from_df(index_array, df)

    # Agrupa con K-Means
    df['group'] = group_using_k_means(genes_to_fit).labels_

    # Separa en ambos grupos y grafica las curvas Kaplan-Meier
    group_1, group_2 = get_groups(
        df,
        event_field=EVENT_FIELD,
        event_living_value=EVENT_LIVING_VALUE,
        event_deceased_value=EVEN_DECEASED_VALUE
    )

    log_rank = compute_survival_curve(
        group_1[TIME_FIELD],
        pd.to_numeric(group_1[EVENT_FIELD]),  # Paso a numerico para evitar un warning
        group_2[TIME_FIELD],
        pd.to_numeric(group_2[EVENT_FIELD]),  # Paso a numerico para evitar un warning,
        plot=plot
    )

    return log_rank


def binary_black_hole_fitness_function(index_array: np.array, genes: pd.DataFrame) -> float:
    if not np.count_nonzero(index_array):
        return -1
    return compute_log_rank(index_array, genes).test_statistic


def main():
    # Hacemos una mini prueba con todos los genes
    # Lee los archivos CSV

    gen_df, clinical_df = get_small_dataset()
    all_genes = gen_df.columns.values

    # Hace un join con clinical data
    merged = gen_df.join(clinical_df, how='inner', lsuffix='_caller', rsuffix='_other')

    # +++++++++++++++++++++++++ METAHEURISTICA BINARY BLACK HOLE +++++++++++++++++++++++++

    best_subset, best_fitness = binary_black_hole(
        n_stars=10,
        n_features=all_genes.shape[0],
        n_iterations=25,
        fitness_function=lambda subset: binary_black_hole_fitness_function(subset, merged)
    )

    # NOTA: acá se está re calculando el K-means, puede no ser el mismo cluster ni los mismos
    # estadísticos que se probaron en el Black Hole.
    black_hole_selected_genes = get_columns_by_categorical(best_subset, merged).columns.values
    print(f'Subset optimo utilizando el Binary Black Hole es -> {black_hole_selected_genes} '
          f'con un test estadistico -> {best_fitness}')
    compute_log_rank(best_subset, merged, plot=True)
    exit()

    # +++++++++++++++++++++++++ REDUCCION DE DIMENSIONALIDAD CON FILTERS +++++++++++++++++++++++++

    df_all_genes = merged[all_genes]
    is_norm_distributed = is_normally_distributed(df_all_genes)
    method = 'pearson' if is_norm_distributed else 'spearman'  # Pearson necesita que esten distribuidos normalmente
    most_related_pairs = get_most_related_pairs(df_all_genes, method=method)
    filtered = filter_by_corr(df_all_genes, most_related_pairs, threshold=0.5)
    print(f'Is normally distributed -> {is_norm_distributed}')
    print(f'Most related pairs -> {most_related_pairs}')
    print(f'After filter -> {filtered}')

    # +++++++++++++++++++++++++ PRUEBA CON TODOS LOS GENES +++++++++++++++++++++++++
    log_rank = compute_log_rank(all_genes, merged)
    print(f'La combinacion de todos los genes arrojo un pvalue -> {log_rank.p_value}')

    # +++++++++++++++++++++++++ PRUEBA CON SPLITS +++++++++++++++++++++++++

    # Obtengo los datos de trainnnig, validacion y testing
    merged_without_group = gen_df.join(clinical_df, how='inner', lsuffix='_caller', rsuffix='_other')
    train, val, test = get_trainning_test_validation_split(merged_without_group)

    # Entreno un modelo usando los datos de entrenamiento
    model = group_using_k_means(train[all_genes])

    # Imprimo la informacion obtenida por el modelo para los datos de testing y validacion
    p_value_test, test_statistic_test = get_logrank_info(
        model, test, all_genes, EVENT_FIELD, TIME_FIELD, EVENT_LIVING_VALUE, EVEN_DECEASED_VALUE
    )
    p_value_val, test_statistic_val = get_logrank_info(
        model, val, all_genes, EVENT_FIELD, TIME_FIELD, EVENT_LIVING_VALUE, EVEN_DECEASED_VALUE
    )

    print('La combinacion de todos los genes arrojo\n'
          f'\tUn pvalue -> {p_value_test} y un estadistico {test_statistic_test} en test\n'
          f'\tUn pvalue -> {p_value_val} y un estadistico {test_statistic_val} en validacion')

    # +++++++++++++++++++++++++ PRUEBA CON LOS GENES OPTIMOS DEL BLIND SEARCH +++++++++++++++++++++++++

    # Para mostrar la lista de genes optima
    # analyse_genes_list(merged, ['HFE', 'DUSP12', 'RNF10', 'RNF11', 'RNF13', 'GTF2IP1', 'MTVR2'],
    #                    time_field=time_field,
    #                    event_field=event_field,
    #                    event_living_value=event_living_value,
    #                    event_deceased_value=event_deceased_value)

    # +++++++++++++++++++++++++ PRUEBA CON BLIND SEARCH (DATASETS SEPARADOS) +++++++++++++++++++++++++
    # Instanciamos la optimizacion que queramos
    optimization = get_optimization(OPTIMIZATION_METHOD)

    blind_search(
        gen_df,
        clinical_df,
        time_field=TIME_FIELD,
        event_field=EVENT_FIELD,
        event_living_value=EVENT_LIVING_VALUE,
        event_deceased_value=EVEN_DECEASED_VALUE,
        optimization=optimization,
        split=True,  # Esta es la linea que indica que se divida en train, val, test
        max_n_combinations=MAX_N_COMBINATIONS
    )

    # +++++++++++++++++++++++++ PRUEBA CON BLIND SEARCH (DATASET COMPLETO) +++++++++++++++++++++++++
    # Blind Search: hacemos una busqueda de todos los casos para obtener la mejor combinacion. Usamos la
    # misma optimizacion que en la busqueda anterior
    best_p_value, best_genes, best_kaplan_meier, best_model = blind_search(
        gen_df,
        clinical_df,
        time_field=TIME_FIELD,
        event_field=EVENT_FIELD,
        event_living_value=EVENT_LIVING_VALUE,
        event_deceased_value=EVEN_DECEASED_VALUE,
        optimization=optimization,
        max_n_combinations=MAX_N_COMBINATIONS
    )

    print(f'La combinacion de genes que mas separan a la curva es -> {best_genes} '
          f'({optimization.score_name} -> {best_p_value})')

    # Grafico la mejor separacion
    compute_survival_curve(best_kaplan_meier[0], best_kaplan_meier[1], best_kaplan_meier[2], best_kaplan_meier[3])


if __name__ == '__main__':
    main()
