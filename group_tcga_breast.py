# Este archivo contiene algoritmos de agrupamiento, Feature Selection y mas aplicados sobre el
# Dataset sacado de cBioPortal (link: http://download.cbioportal.org/brca_tcga_pub.tar.gz)
# Dicho dataset contiene 16806 genes y mas de 500 pacientes, lo que lo hace interesante para la aplicacion de
# metaeuristicas y otras optimizaciones
import pandas as pd
from typing import Optional
from datasets import get_big_dataset
from optimizations import get_optimization
from util import compute_survival_curve, blind_search, get_groups, group_using_k_means

# Maxima cantidad de elementos a combinar, si se pone en None se realizan todas las combinaciones posibles
MAX_N_COMBINATIONS: Optional[int] = None

# Cantidad de valores NaNs debe tener al menos un gen para que sea eliminado.
# Si el gen no es eliminado porque no llega al threshold, se elimina al paciente
# FIXME: todavia no se utiliza
THRESHOLD_NA_VALUES_COUNT: int = 2


def main():
    # Lee los archivos CSV
    gen_df, clinical_df = get_big_dataset()
    all_genes = gen_df.columns.values

    # Hace un join con clinical data
    merged = gen_df.join(clinical_df, how='inner', lsuffix='_caller', rsuffix='_other')

    # Agrupa con K-Means
    merged['group'] = group_using_k_means(merged[all_genes]).labels_

    # Imprimo un poco de informacion con respecto a la cantidad de genes que manejamos y a la cantidad de samples
    # que quedaron despues de hacer el inner join
    print(f'El dataset final tiene {all_genes.shape[0]} genes y {merged.shape[0]} samples')

    # Seteamos algunos datos del dataset para manejar la informacion
    time_field = 'OS_MONTHS'
    event_field = 'OS_STATUS'
    event_living_value = 'LIVING'
    event_deceased_value = 'DECEASED'

    # Separa en ambos grupos y grafica las curvas Kaplan-Meier
    group_1, group_2 = get_groups(
        merged,
        event_field=event_field,
        event_living_value=event_living_value,
        event_deceased_value=event_deceased_value
    )

    log_rank = compute_survival_curve(
        group_1[time_field],
        pd.to_numeric(group_1[event_field]),  # Paso a numerico para evitar un warning
        group_2[time_field],
        pd.to_numeric(group_2[event_field])  # Paso a numerico para evitar un warning
    )

    print(f'La combinacion de todos los genes arrojo un pvalue -> {log_rank.p_value}')

    # Blind Search: hacemos una busqueda de todos los casos para obtener la mejor combinacio
    optimization = get_optimization('logrank_p-value')
    best_p_value, best_genes, best_kaplan_meier, best_model = blind_search(
        gen_df,
        clinical_df,
        time_field=time_field,
        event_field=event_field,
        event_living_value=event_living_value,
        event_deceased_value=event_deceased_value,
        optimization=optimization,
        max_n_combinations=MAX_N_COMBINATIONS
    )

    print(f'La combinacion de genes que mas separan a la curva es -> {best_genes} (pvalue -> {best_p_value})')

    # Grafico la mejor separacion
    compute_survival_curve(best_kaplan_meier[0], best_kaplan_meier[1], best_kaplan_meier[2], best_kaplan_meier[3])


if __name__ == '__main__':
    main()
