import pandas as pd
from lifelines import *
import matplotlib.pyplot as plt
from lifelines.statistics import logrank_test

x = [0]
y = [1]

# Treatment: 1 = treated, 0 = Placebo
# t: Time of remission in weeks
# status: 1 = relapsed, 0 = censored
# ref: Reference Freireich et al. Blood 21(1963)
df = pd.DataFrame({'Treatment': 21 * x + 21 * y,
                   't': [1, 1, 2, 2, 3, 4, 4, 5, 5, 8, 8, 8, 8, 11, 11, 12, 12, 15, 17, 22, 23, 6, 6, 6, 6, 7, 9, 10,
                         10, 11, 13, 16, 17, 19, 20, 22, 23, 25, 32, 32, 34, 35],
                   'status': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0,
                              1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0]})

print(df)
exit()

# KM curve
group1 = df[df['Treatment'] == 1]
group2 = df[df['Treatment'] == 0]
T = group1['t']
E = group1['status']
T1 = group2['t']
E1 = group2['status']

kmf = KaplanMeierFitter()

ax = plt.subplot(111)
ax = kmf.fit(T, E, label="Group 1-Treatment").plot(ax=ax)
ax = kmf.fit(T1, E1, label="Group 2 - Placebo").plot(ax=ax)

# logrank_test
results = logrank_test(T, T1, event_observed_A=E, event_observed_B=E1)
results.print_summary()

plt.show()
