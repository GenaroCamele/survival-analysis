import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import stats

x = [0]
y = [1]

# Treatment: 1 = treated, 0 = Placebo
# t: Time of remission in weeks
# status: 1 = relapsed, 0 = censored
# ref: Reference Freireich et al. Blood 21(1963)
df = pd.DataFrame({'Treatment': 21 * x + 21 * y,
                   't': [1, 1, 2, 2, 3, 4, 4, 5, 5, 8, 8, 8, 8, 11, 11, 12, 12, 15, 17, 22, 23, 6, 6, 6, 6, 7, 9, 10,
                         10, 11, 13, 16, 17, 19, 20, 22, 23, 25, 32, 32, 34, 35],
                   'status': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0,
                              1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0]})

print(df)

# Meaningful info
df.groupby('Treatment').mean()
pd.crosstab(df.Treatment, df.status)
pd.crosstab(df.status, df.Treatment, rownames=['status'], colnames=['Treatment'])
sns.heatmap([df.Treatment, df.status], cmap="YlGnBu", annot=True, cbar=False)
plt.plot()
g = sns.FacetGrid(df, row="Treatment", col="status")
g.map(plt.hist, 'status')
plt.plot()
v = sns.FacetGrid(df, row='status', col='Treatment')
v.map(plt.hist, 't')
df.groupby('Treatment')['t'].plot()
plt.plot()

mask = (df['Treatment'] == 1) & (df['status'] == 0)
mask1 = (df['Treatment'] == 1) & (df['status'] == 1)
plt.figure()
df.groupby(['Treatment', 'status'])['t'].plot()


# Normality tests
def normal(col):
    statistic, p_value = stats.kstest(col, 'norm')
    print(f'Statistics: {statistic}, p_value:{p_value}')
    if p_value < 0.05:
        return print('The distribution differs from normal')
    else:
        return print('The distribution is normal')


normal(df[mask1]['t'])
normal(df[mask]['t'])


def skew_kurt(col):
    return print(
        f'The group  who received treatment and censored \n Skewness:{stats.skew(col)} Kurtosis:{stats.kurtosis(col)}'
    )


skew_kurt(df[mask]['t'])
skew_kurt(df[mask1]['t'])
