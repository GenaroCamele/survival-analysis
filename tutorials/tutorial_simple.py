from lifelines import KaplanMeierFitter
import matplotlib.pyplot as plt
import pandas as pd
from lifelines.statistics import logrank_test
from lifelines import CoxPHFitter

# Example Data
durations = [5, 6, 6, 2.5, 4, 4]
event_observed = [1, 0, 0, 1, 1, 1]

# create a kmf object
kmf = KaplanMeierFitter()

# Fit the data into the model
kmf.fit(durations, event_observed, label='Kaplan Meier Estimate')

# Create an estimate
# kmf.plot(
#     ci_show=False)  # ci_show is meant for Confidence interval, since our data set is too tiny, thus i am not showing it.
# plt.show()

# Ejemplo con dos grupos distintos
kmf1 = KaplanMeierFitter()  # instantiate the class to create an object

# # Two Cohorts are compared. Cohort 1. Streaming TV Not Subscribed by users, and Cohort  2. Streaming TV subscribed
# by the users.
df = pd.read_csv('./WA_Fn-UseC_-Telco-Customer-Churn.csv')

# print(df.head())
# df.info()

# Formateamos algunos campos
df['TotalCharges'] = pd.to_numeric(df['TotalCharges'], errors='coerce')

# Replace yes and No in the Churn column to 1 and 0. 1 for the event and 0 for the censured data.
df['Churn'] = df['Churn'].apply(lambda x: 1 if x == 'Yes' else 0)

# Impute the null value with the median value
df.TotalCharges.fillna(value=df['TotalCharges'].median(), inplace=True)

# Create a list of Categorical Columns
cat_cols = [i for i in df.columns if df[i].dtype == object]
cat_cols.remove('customerID')  # customerID has been removed because it is unique for all the rows.

# KAPLAN - MEIER!

kmf1 = KaplanMeierFitter()  # instantiate the class to create an object
T = df['tenure']  # time to event
E = df['Churn']  # event occurred or censored

# Two Cohorts are compared. 1. Streaming TV Not Subsribed by Users, 2. Streaming TV subscribed by the users.
groups = df['StreamingTV']
i1 = (groups == 'No')  # group i1 , having the pandas series for the 1st cohort
i2 = (groups == 'Yes')  # group i2 , having the pandas series for the 2nd cohort

# fit the model for 1st cohort
kmf1.fit(T[i1], E[i1], label='Not Subscribed StreamingTV')
# a1 = kmf1.plot()

# fit the model for 2nd cohort
kmf1.fit(T[i2], E[i2], label='Subscribed StreamingTV')
# kmf1.plot(ax=a1)
# plt.show()

# LOGRANK
# Cuanto menor sea el p-value mejor, ya que indica la probabilidad de que se cumpla la
# hipotesis nula de que ambas curvas tienen la misma probabilidad
results = logrank_test(T[i1], T[i2], event_observed_A=T[i1], event_observed_B=T[i2])
results.print_summary()
print(results.p_value)

# COX REGRESSION
# My objective here is to introduce you to the implementation of the model. Thus taking subset of the columns to train
# the model. Only using the subset of the columns present in the original data
df_r = df[['tenure', 'Churn', 'gender', 'Partner', 'Dependents', 'PhoneService', 'MonthlyCharges', 'SeniorCitizen',
                'StreamingTV']]

df_dummy = pd.get_dummies(df_r, drop_first=True)
# print(df_dummy.head())

cph = CoxPHFitter()   # Instantiate the class to create a cph object
cph.fit(df_dummy, 'tenure', event_col='Churn')   # Fit the data to train the model
cph.print_summary()    # Have a look at the significance of the features
cph.plot()
plt.show()

# Para ver a nivel de cliente
tr_rows = df_dummy.iloc[5:10, 2:]
cph.predict_survival_function(tr_rows).plot()
plt.show()

# Buscamos un score mejor!
df_dummy1 = df_dummy

