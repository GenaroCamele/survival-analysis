## TCGA cBioPortal:

La combinacion de todos los genes arrojo un pvalue -> 0.7542062341535222
Probando combinaciones con 1 genes de 16806 genes totales
Busqueda con todas las combinaciones de 1 genes terminada en 1241.3627016544342 segundos
Mejor combinacion hasta ahora -> ['KCNJ5'] (pvalue -> 8.625348876950706e-06)
Probando combinaciones con 2 genes de 16806 genes totales... No terminó!

## Propio (15 genes):

El dataset final tiene 16 genes y 141 samples
La combinacion de todos los genes arrojo un pvalue -> 0.9799399669229785
Probando combinaciones con 1 genes de 16 genes totales
Busqueda con todas las combinaciones de 1 genes terminada en 1.2550578117370605 segundos
Mejor combinacion hasta ahora -> ['GTF2IP1'] (pvalue -> 0.004907087675846225)
Probando combinaciones con 2 genes de 16 genes totales
Busqueda con todas las combinaciones de 2 genes terminada en 7.987894058227539 segundos
Mejor combinacion hasta ahora -> ['DUSP12', 'RNF13'] (pvalue -> 0.0005354049989455717)
Probando combinaciones con 3 genes de 16 genes totales
Busqueda con todas las combinaciones de 3 genes terminada en 41.2269287109375 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'RNF13'] (pvalue -> 0.00013407438120635907)
Probando combinaciones con 4 genes de 16 genes totales
Busqueda con todas las combinaciones de 4 genes terminada en 133.757390499115 segundos
Mejor combinacion hasta ahora -> ['HFE', 'NUBPL', 'RNF11', 'RNF13'] (pvalue -> 2.333358318149639e-06)
Probando combinaciones con 5 genes de 16 genes totales
Busqueda con todas las combinaciones de 5 genes terminada en 319.0756034851074 segundos
Mejor combinacion hasta ahora -> ['HFE', 'NUBPL', 'RNF11', 'RNF13'] (pvalue -> 2.333358318149639e-06)
Probando combinaciones con 6 genes de 16 genes totales
Busqueda con todas las combinaciones de 6 genes terminada en 581.6974310874939 segundos
Mejor combinacion hasta ahora -> ['HFE', 'ARHGEF10L', 'RNF10', 'RNF11', 'RNF13', 'MTVR2'] (pvalue -> 9.1342078598563e-07)
Probando combinaciones con 7 genes de 16 genes totales
Busqueda con todas las combinaciones de 7 genes terminada en 803.158319234848 segundos
Mejor combinacion hasta ahora -> ['HFE', 'ARHGEF10L', 'RNF10', 'RNF11', 'RNF13', 'MTVR2'] (pvalue -> 9.1342078598563e-07)
Probando combinaciones con 8 genes de 16 genes totales
Busqueda con todas las combinaciones de 8 genes terminada en 948.6737926006317 segundos
Mejor combinacion hasta ahora -> ['HFE', 'ARHGEF10L', 'RNF10', 'RNF11', 'RNF13', 'MTVR2'] (pvalue -> 9.1342078598563e-07)
Probando combinaciones con 9 genes de 16 genes totales
Busqueda con todas las combinaciones de 9 genes terminada en 843.1646511554718 segundos
Mejor combinacion hasta ahora -> ['HFE', 'ARHGEF10L', 'RNF10', 'RNF11', 'RNF13', 'MTVR2'] (pvalue -> 9.1342078598563e-07)
Probando combinaciones con 10 genes de 16 genes totales
Busqueda con todas las combinaciones de 10 genes terminada en 579.6115682125092 segundos
Mejor combinacion hasta ahora -> ['HFE', 'ARHGEF10L', 'RNF10', 'RNF11', 'RNF13', 'MTVR2'] (pvalue -> 9.1342078598563e-07)
Probando combinaciones con 11 genes de 16 genes totales
Busqueda con todas las combinaciones de 11 genes terminada en 316.2682132720947 segundos
Mejor combinacion hasta ahora -> ['HFE', 'ARHGEF10L', 'RNF10', 'RNF11', 'RNF13', 'MTVR2'] (pvalue -> 9.1342078598563e-07)
Probando combinaciones con 12 genes de 16 genes totales
Busqueda con todas las combinaciones de 12 genes terminada en 141.0714180469513 segundos
Mejor combinacion hasta ahora -> ['HFE', 'ARHGEF10L', 'RNF10', 'RNF11', 'RNF13', 'MTVR2'] (pvalue -> 9.1342078598563e-07)
Probando combinaciones con 13 genes de 16 genes totales
Busqueda con todas las combinaciones de 13 genes terminada en 40.03676414489746 segundos
Mejor combinacion hasta ahora -> ['HFE', 'ARHGEF10L', 'RNF10', 'RNF11', 'RNF13', 'MTVR2'] (pvalue -> 9.1342078598563e-07)
Probando combinaciones con 14 genes de 16 genes totales
Busqueda con todas las combinaciones de 14 genes terminada en 9.077639818191528 segundos
Mejor combinacion hasta ahora -> ['HFE', 'ARHGEF10L', 'RNF10', 'RNF11', 'RNF13', 'MTVR2'] (pvalue -> 9.1342078598563e-07)
Probando combinaciones con 15 genes de 16 genes totales
Busqueda con todas las combinaciones de 15 genes terminada en 1.3635408878326416 segundos
Mejor combinacion hasta ahora -> ['HFE', 'ARHGEF10L', 'RNF10', 'RNF11', 'RNF13', 'MTVR2'] (pvalue -> 9.1342078598563e-07)
Probando combinaciones con 16 genes de 16 genes totales
Busqueda con todas las combinaciones de 16 genes terminada en 0.06694984436035156 segundos
Mejor combinacion hasta ahora -> ['HFE', 'ARHGEF10L', 'RNF10', 'RNF11', 'RNF13', 'MTVR2'] (pvalue -> 9.1342078598563e-07)
Blind search terminada en 4767.493835449219 segundos
La combinacion de genes que mas separan a la curva es -> ['HFE', 'ARHGEF10L', 'RNF10', 'RNF11', 'RNF13', 'MTVR2'] (pvalue -> 9.1342078598563e-07)

## Propio con 15 genes (haciendo un merge una unica vez):

El dataset final tiene 15 genes y 141 samples
La combinacion de todos los genes arrojo un pvalue -> 0.9208184477096867
Probando combinaciones con 1 genes de 15 genes totales
Busqueda con todas las combinaciones de 1 genes terminada en 0.9853384494781494 segundos
Mejor combinacion hasta ahora -> ['GTF2IP1'] (pvalue -> 0.004907087675846225)
Probando combinaciones con 2 genes de 15 genes totales
Busqueda con todas las combinaciones de 2 genes terminada en 6.4056806564331055 segundos
Mejor combinacion hasta ahora -> ['HFE', 'RNF13'] (pvalue -> 0.00019104065681386058)
Probando combinaciones con 3 genes de 15 genes totales
Busqueda con todas las combinaciones de 3 genes terminada en 28.706193208694458 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'RNF13'] (pvalue -> 0.00013407438120635907)
Probando combinaciones con 4 genes de 15 genes totales
Busqueda con todas las combinaciones de 4 genes terminada en 98.75968956947327 segundos
Mejor combinacion hasta ahora -> ['ARHGEF10L', 'RNF11', 'RNF13', 'GTF2IP1'] (pvalue -> 9.823613909088684e-05)
Probando combinaciones con 5 genes de 15 genes totales
Busqueda con todas las combinaciones de 5 genes terminada en 238.90190315246582 segundos
Mejor combinacion hasta ahora -> ['HFE', 'ARHGEF10L', 'RNF10', 'RNF13', 'MTVR2'] (pvalue -> 8.820719126879473e-06)
Probando combinaciones con 6 genes de 15 genes totales
Busqueda con todas las combinaciones de 6 genes terminada en 356.72330713272095 segundos
Mejor combinacion hasta ahora -> ['HFE', 'ARHGEF10L', 'RNF10', 'RNF13', 'MTVR2'] (pvalue -> 8.820719126879473e-06)
Probando combinaciones con 7 genes de 15 genes totales
Busqueda con todas las combinaciones de 7 genes terminada en 426.4819791316986 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'ARHGEF10L', 'RNF17', 'RNF11', 'RNF13', 'GTF2IP1'] (pvalue -> 3.124687789201057e-07)
Probando combinaciones con 8 genes de 15 genes totales
Busqueda con todas las combinaciones de 8 genes terminada en 425.91980719566345 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'ARHGEF10L', 'RNF17', 'RNF11', 'RNF13', 'GTF2IP1'] (pvalue -> 3.124687789201057e-07)
Probando combinaciones con 9 genes de 15 genes totales
Busqueda con todas las combinaciones de 9 genes terminada en 408.53488779067993 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'ARHGEF10L', 'RNF17', 'RNF11', 'RNF13', 'GTF2IP1'] (pvalue -> 3.124687789201057e-07)
Probando combinaciones con 10 genes de 15 genes totales
Busqueda con todas las combinaciones de 10 genes terminada en 210.1429557800293 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'ARHGEF10L', 'RNF17', 'RNF11', 'RNF13', 'GTF2IP1'] (pvalue -> 3.124687789201057e-07)
Probando combinaciones con 11 genes de 15 genes totales
Busqueda con todas las combinaciones de 11 genes terminada en 96.56056547164917 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'ARHGEF10L', 'RNF17', 'RNF11', 'RNF13', 'GTF2IP1'] (pvalue -> 3.124687789201057e-07)
Probando combinaciones con 12 genes de 15 genes totales
Busqueda con todas las combinaciones de 12 genes terminada en 33.15508270263672 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'ARHGEF10L', 'RNF17', 'RNF11', 'RNF13', 'GTF2IP1'] (pvalue -> 3.124687789201057e-07)
Probando combinaciones con 13 genes de 15 genes totales
Busqueda con todas las combinaciones de 13 genes terminada en 9.484729290008545 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'ARHGEF10L', 'RNF17', 'RNF11', 'RNF13', 'GTF2IP1'] (pvalue -> 3.124687789201057e-07)
Probando combinaciones con 14 genes de 15 genes totales
Busqueda con todas las combinaciones de 14 genes terminada en 0.9847040176391602 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'ARHGEF10L', 'RNF17', 'RNF11', 'RNF13', 'GTF2IP1'] (pvalue -> 3.124687789201057e-07)
Probando combinaciones con 15 genes de 15 genes totales
Busqueda con todas las combinaciones de 15 genes terminada en 0.06796526908874512 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'ARHGEF10L', 'RNF17', 'RNF11', 'RNF13', 'GTF2IP1'] (pvalue -> 3.124687789201057e-07)
Blind search terminada en 2341.8153698444366 segundos
La combinacion de genes que mas separan a la curva es -> ['HFE', 'DUSP12', 'ARHGEF10L', 'RNF17', 'RNF11', 'RNF13', 'GTF2IP1'] (pvalue -> 3.124687789201057e-07)

## Propio (15 genes) con datasets de validación y testing

El dataset final tiene 15 genes y 141 samples
La combinacion de todos los genes arrojo un pvalue -> 0.9208184477096867
La combinacion de todos los genes arrojo
	Un pvalue -> 0.8555749306166501 y un estadistico 0.033127615749288936 en test
	Un pvalue -> 0.516501087237422 y un estadistico 0.4208743410358617 en validacion
Probando combinaciones con 1 genes de 15 genes totales
Busqueda con todas las combinaciones de 1 genes terminada en 1.1021251678466797 segundos
Mejor combinacion hasta ahora -> ['RNF13'] (p-value -> 1.3702562776310388e-05)
Dicha combinación arrojó
	Un pvalue -> 0.21315028429885405 y un estadístico 1.5498994573872973 en test
	Un pvalue -> 0.8769168094276538 y un estadistico 0.023987465352804143 en validación
Probando combinaciones con 2 genes de 15 genes totales
Busqueda con todas las combinaciones de 2 genes terminada en 8.906840801239014 segundos
Mejor combinacion hasta ahora -> ['RNF13'] (p-value -> 1.3702562776310388e-05)
Dicha combinación arrojó
	Un pvalue -> 0.21315028429885405 y un estadístico 1.5498994573872973 en test
	Un pvalue -> 0.8769168094276538 y un estadistico 0.023987465352804143 en validación
Probando combinaciones con 3 genes de 15 genes totales
Busqueda con todas las combinaciones de 3 genes terminada en 33.79662537574768 segundos
Mejor combinacion hasta ahora -> ['RNF13'] (p-value -> 1.3702562776310388e-05)
Dicha combinación arrojó
	Un pvalue -> 0.21315028429885405 y un estadístico 1.5498994573872973 en test
	Un pvalue -> 0.8769168094276538 y un estadistico 0.023987465352804143 en validación
Probando combinaciones con 4 genes de 15 genes totales
Busqueda con todas las combinaciones de 4 genes terminada en 86.12996697425842 segundos
Mejor combinacion hasta ahora -> ['RNF13'] (p-value -> 1.3702562776310388e-05)
Dicha combinación arrojó
	Un pvalue -> 0.21315028429885405 y un estadístico 1.5498994573872973 en test
	Un pvalue -> 0.8769168094276538 y un estadistico 0.023987465352804143 en validación
Probando combinaciones con 5 genes de 15 genes totales
Busqueda con todas las combinaciones de 5 genes terminada en 189.655992269516 segundos
Mejor combinacion hasta ahora -> ['RNF13'] (p-value -> 1.3702562776310388e-05)
Dicha combinación arrojó
	Un pvalue -> 0.21315028429885405 y un estadístico 1.5498994573872973 en test
	Un pvalue -> 0.8769168094276538 y un estadistico 0.023987465352804143 en validación
Probando combinaciones con 6 genes de 15 genes totales
Busqueda con todas las combinaciones de 6 genes terminada en 330.20086765289307 segundos
Mejor combinacion hasta ahora -> ['HFE', 'NUBPL', 'ARHGEF10L', 'RNF10', 'RNF11', 'RNF13'] (p-value -> 1.3351867469485048e-05)
Dicha combinación arrojó
	Un pvalue -> 0.3079307980689614 y un estadístico 1.03953003693328 en test
	Un pvalue -> 0.5017347651480102 y un estadistico 0.45126515252226573 en validación
Probando combinaciones con 7 genes de 15 genes totales
Busqueda con todas las combinaciones de 7 genes terminada en 426.433073759079 segundos
Mejor combinacion hasta ahora -> ['HFE', 'NUBPL', 'ARHGEF10L', 'RNF10', 'RNF11', 'RNF13'] (p-value -> 1.3351867469485048e-05)
Dicha combinación arrojó
	Un pvalue -> 0.3079307980689614 y un estadístico 1.03953003693328 en test
	Un pvalue -> 0.5017347651480102 y un estadistico 0.45126515252226573 en validación
Probando combinaciones con 8 genes de 15 genes totales
Busqueda con todas las combinaciones de 8 genes terminada en 493.8793339729309 segundos
Mejor combinacion hasta ahora -> ['HFE', 'NUBPL', 'ARHGEF10L', 'RNF10', 'RNF11', 'RNF13'] (p-value -> 1.3351867469485048e-05)
Dicha combinación arrojó
	Un pvalue -> 0.3079307980689614 y un estadístico 1.03953003693328 en test
	Un pvalue -> 0.5017347651480102 y un estadistico 0.45126515252226573 en validación
Probando combinaciones con 9 genes de 15 genes totales
Busqueda con todas las combinaciones de 9 genes terminada en 439.38461470603943 segundos
Mejor combinacion hasta ahora -> ['HFE', 'NUBPL', 'ARHGEF10L', 'RNF10', 'RNF11', 'RNF13'] (p-value -> 1.3351867469485048e-05)
Dicha combinación arrojó
	Un pvalue -> 0.3079307980689614 y un estadístico 1.03953003693328 en test
	Un pvalue -> 0.5017347651480102 y un estadistico 0.45126515252226573 en validación
Probando combinaciones con 10 genes de 15 genes totales
Busqueda con todas las combinaciones de 10 genes terminada en 273.5974862575531 segundos
Mejor combinacion hasta ahora -> ['HFE', 'NUBPL', 'ARHGEF10L', 'RNF10', 'RNF11', 'RNF13'] (p-value -> 1.3351867469485048e-05)
Dicha combinación arrojó
	Un pvalue -> 0.3079307980689614 y un estadístico 1.03953003693328 en test
	Un pvalue -> 0.5017347651480102 y un estadistico 0.45126515252226573 en validación
Probando combinaciones con 11 genes de 15 genes totales
Busqueda con todas las combinaciones de 11 genes terminada en 131.9189031124115 segundos
Mejor combinacion hasta ahora -> ['HFE', 'NUBPL', 'ARHGEF10L', 'RNF10', 'RNF11', 'RNF13'] (p-value -> 1.3351867469485048e-05)
Dicha combinación arrojó
	Un pvalue -> 0.3079307980689614 y un estadístico 1.03953003693328 en test
	Un pvalue -> 0.5017347651480102 y un estadistico 0.45126515252226573 en validación
Probando combinaciones con 12 genes de 15 genes totales
Busqueda con todas las combinaciones de 12 genes terminada en 41.32674527168274 segundos
Mejor combinacion hasta ahora -> ['HFE', 'NUBPL', 'ARHGEF10L', 'RNF10', 'RNF11', 'RNF13'] (p-value -> 1.3351867469485048e-05)
Dicha combinación arrojó
	Un pvalue -> 0.3079307980689614 y un estadístico 1.03953003693328 en test
	Un pvalue -> 0.5017347651480102 y un estadistico 0.45126515252226573 en validación
Probando combinaciones con 13 genes de 15 genes totales
Busqueda con todas las combinaciones de 13 genes terminada en 11.887199640274048 segundos
Mejor combinacion hasta ahora -> ['HFE', 'NUBPL', 'ARHGEF10L', 'RNF10', 'RNF11', 'RNF13'] (p-value -> 1.3351867469485048e-05)
Dicha combinación arrojó
	Un pvalue -> 0.3079307980689614 y un estadístico 1.03953003693328 en test
	Un pvalue -> 0.5017347651480102 y un estadistico 0.45126515252226573 en validación
Probando combinaciones con 14 genes de 15 genes totales
Busqueda con todas las combinaciones de 14 genes terminada en 1.5406970977783203 segundos
Mejor combinacion hasta ahora -> ['HFE', 'NUBPL', 'ARHGEF10L', 'RNF10', 'RNF11', 'RNF13'] (p-value -> 1.3351867469485048e-05)
Dicha combinación arrojó
	Un pvalue -> 0.3079307980689614 y un estadístico 1.03953003693328 en test
	Un pvalue -> 0.5017347651480102 y un estadistico 0.45126515252226573 en validación
Probando combinaciones con 15 genes de 15 genes totales
Busqueda con todas las combinaciones de 15 genes terminada en 0.12574982643127441 segundos
Mejor combinacion hasta ahora -> ['HFE', 'NUBPL', 'ARHGEF10L', 'RNF10', 'RNF11', 'RNF13'] (p-value -> 1.3351867469485048e-05)
Dicha combinación arrojó
	Un pvalue -> 0.3079307980689614 y un estadístico 1.03953003693328 en test
	Un pvalue -> 0.5017347651480102 y un estadistico 0.45126515252226573 en validación
Blind search terminada en 2471.6122002601624 segundos
Probando combinaciones con 1 genes de 15 genes totales
Busqueda con todas las combinaciones de 1 genes terminada en 1.2670092582702637 segundos
Mejor combinacion hasta ahora -> ['HFE'] (p-value -> 0.004121957636469873)
Probando combinaciones con 2 genes de 15 genes totales
Busqueda con todas las combinaciones de 2 genes terminada en 9.926647901535034 segundos
Mejor combinacion hasta ahora -> ['RNF10', 'GTF2IP1'] (p-value -> 0.0014377496675668112)
Probando combinaciones con 3 genes de 15 genes totales
Busqueda con todas las combinaciones de 3 genes terminada en 47.949846029281616 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'RNF13'] (p-value -> 0.00013407438120636734)
Probando combinaciones con 4 genes de 15 genes totales
Busqueda con todas las combinaciones de 4 genes terminada en 140.92077660560608 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'RNF17', 'MTVR2'] (p-value -> 8.27486820051582e-05)
Probando combinaciones con 5 genes de 15 genes totales
Busqueda con todas las combinaciones de 5 genes terminada en 299.9924120903015 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'NUBPL', 'ARHGEF10L', 'MTVR2'] (p-value -> 4.687737926514876e-05)
Probando combinaciones con 6 genes de 15 genes totales
Busqueda con todas las combinaciones de 6 genes terminada en 518.3294832706451 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'ARHGEF10L', 'RNF11', 'RNF13', 'GTF2IP1'] (p-value -> 3.124687789201057e-07)
Probando combinaciones con 7 genes de 15 genes totales
Busqueda con todas las combinaciones de 7 genes terminada en 672.8367986679077 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'ARHGEF10L', 'RNF11', 'RNF13', 'GTF2IP1'] (p-value -> 3.124687789201057e-07)
Probando combinaciones con 8 genes de 15 genes totales
Busqueda con todas las combinaciones de 8 genes terminada en 650.3838489055634 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'ARHGEF10L', 'RNF11', 'RNF13', 'GTF2IP1'] (p-value -> 3.124687789201057e-07)
Probando combinaciones con 9 genes de 15 genes totales
Busqueda con todas las combinaciones de 9 genes terminada en 533.6371049880981 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'ARHGEF10L', 'RNF11', 'RNF13', 'GTF2IP1'] (p-value -> 3.124687789201057e-07)
Probando combinaciones con 10 genes de 15 genes totales
Busqueda con todas las combinaciones de 10 genes terminada en 204.52013683319092 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'ARHGEF10L', 'RNF11', 'RNF13', 'GTF2IP1'] (p-value -> 3.124687789201057e-07)
Probando combinaciones con 11 genes de 15 genes totales
Busqueda con todas las combinaciones de 11 genes terminada en 88.95486879348755 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'ARHGEF10L', 'RNF11', 'RNF13', 'GTF2IP1'] (p-value -> 3.124687789201057e-07)
Probando combinaciones con 12 genes de 15 genes totales
Busqueda con todas las combinaciones de 12 genes terminada en 29.58270812034607 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'ARHGEF10L', 'RNF11', 'RNF13', 'GTF2IP1'] (p-value -> 3.124687789201057e-07)
Probando combinaciones con 13 genes de 15 genes totales
Busqueda con todas las combinaciones de 13 genes terminada en 6.9019715785980225 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'ARHGEF10L', 'RNF11', 'RNF13', 'GTF2IP1'] (p-value -> 3.124687789201057e-07)
Probando combinaciones con 14 genes de 15 genes totales
Busqueda con todas las combinaciones de 14 genes terminada en 0.9901096820831299 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'ARHGEF10L', 'RNF11', 'RNF13', 'GTF2IP1'] (p-value -> 3.124687789201057e-07)
Probando combinaciones con 15 genes de 15 genes totales
Busqueda con todas las combinaciones de 15 genes terminada en 0.06287360191345215 segundos
Mejor combinacion hasta ahora -> ['HFE', 'DUSP12', 'ARHGEF10L', 'RNF11', 'RNF13', 'GTF2IP1'] (p-value -> 3.124687789201057e-07)
Blind search terminada en 3206.257210969925 segundos
La combinacion de genes que mas separan a la curva es -> ['HFE', 'DUSP12', 'ARHGEF10L', 'RNF11', 'RNF13', 'GTF2IP1'] (p-value -> 3.124687789201057e-07)


## Propio (15 genes) con Binary Black Hole

Subset optimo utilizando el Binary Black Hole es -> ['DUSP12' 'LOXHD1' 'HIF3A' 'RNF10' 'REM1' 'RTN4RL2'] con un p-value -> 2.5028097719025483e-06
