# Paper

## Introducción

Lo desarrollado acá buscan implementar los algoritmos definidos en los siguientes papers:
1. [Binary black hole algorithm for feature selection and classification on biological data](https://www.sciencedirect.com/science/article/abs/pii/S1568494617301242?via%3Dihub): la metaheurística en sí misma. 
1. [Improved black hole and multiverse algorithms for discrete sizing optimization of planar structures](https://www.tandfonline.com/doi/full/10.1080/0305215X.2018.1540697): el paper que tunea un poco al algoritmo.
1. [A Multi Dynamic Binary Black Hole Algorithm Applied to Set Covering Problem](https://link.springer.com/chapter/10.1007%2F978-981-10-3728-3_6): implementación de una variante del algoritmo en Spark.

Para correr el código hay que instalar las dependencias como se indica en el [README.md](../README.md) principal ya que son las mismas.


## Organización del código

En la carpeta `Datasets` se dejan los archivos .arff correspondientes a los datasets (para más info sobre los mismos ver la sección [Datasets](#datasets)).

El código principal está en el archivo `core.py` donde se ejecuta la metaheurística y se informan y guardan los resultados en un archivo CSV que irá a parar a la carpeta `Resultados`. Tanto `sequential.py` como `spark.py` se definen las funciones y parámetros que se pasa al `core` para ejecutar. De esta manera se pueden correr los experimetos secuenciales (Python plano) y los desarrollados en un entorno Spark de manera limpia ya que ambos enfoques comparten el pre-procesamiento, las corridas independientes y el algoritmo de la metaheurística.

En el archivo `paper_util.py` se encuentran las funciones de importación, de  binarización de la columna del label del dataset, de preprocesamiento, entre otras funciones útiles.

El algoritmo de la metaheurística y sus variantes se pueden encontrar en el archivo `custom_metaheuristics.py`.

El archivo `generate_datasets_csv.py` genera, para cada dataset en formato `.arff` un archivo `.csv`. El fin de este script es generar archivos que sean más fácil de manipular en el contenedor con HDFS y Spark. **Actualmente no se encuentra en uso para mantener los archivos arff originales**

Para el cálculo de las métricas se agrupó todo el código en el archivo `metrics.py`.

En el resto de los archivos hay pruebas y enfoques viejos que no se vienen al caso pero que se dejan por las dudas.

## Algoritmo original y objetivos

El algoritmo que proponen en el paper original es el siguiente:

1. Realizan el Binary Black Hole Algorithm (BBHA) optimizando por **accuracy únicamente** a partir de un Cross Validation de 10 folds.
1. Cuando finaliza el algoritmo de BBHA se hace un promedio de 100 ejecuciones del RandomForest para los features que resultaron, reportando el promedio y la desviación estándar de las 5 métricas que figuran debajo.
1. El proceso de los dos puntos anteriores se corre 5 veces, al final de todo se reporta el promedio y la desviación estándar de las 5 métricas y los 5 tiempos de ejecución de las corridas.
1. Lo que no especifica pero es lo más lógico es que el resultado de la lista de Features finales es la que mejor métrica obtuvo en las 5 ejecuciones del punto anterior.

**Pseudo-código**:

```
for 1 to 5: (corridas independientes)
    subset = BBHA optimizando con el RandomForest por accuracy
    for 1 to 100:
        calculan las 5 metricas con el subset obtenido
    almacenan el promedio de las 5 métricas de las 100 corridas
    almacenan el tiempo total de la corrida independiente
reportan el promedio y desvío std de cada una de las 5 metricas y tiempos de las 5 corridas
```

A continuación se detallan las métricas obtenidas por el paper para cada uno de los datasets. Se deja la información obtenida del RandomForest para poder comparar y evitar errores en los modelos que se usan para evaluar los diferentes puntos del espacio de soluciones. También se dejan el subset de features a los que llegaron para poder comparar.

- Ovarian (253 samples y 15154 features):
    - Métricas para el RandomForest:
        - Accuracy: 99.82 ± 0.34
        - Sentivity: 100 ± 0.0
        - Specificity: 99.58 ± 0.85
        - MCC: 99.69 ± 0.61
        - AUC (ROC): 100 ± 0.0
    - Tiempo de CPU en terminar las 5 corridas: 266.60 segundos
- Leukemia (72 samples y 7129 features):
    - Métricas para el RandomForest:
        - Accuracy: 98.61 ± 1.23
        - Sentivity: 98.88 ± 1.43
        - Specificity: 98.77 ± 1.99
        - MCC: 92.69 ± 1.34
        - AUC (ROC): 96.38 ± 1.38
    - Tiempo de CPU en terminar las 5 corridas: 101.84 segundos
- Breast (97 samples y 24481 features)::
    - Métricas para el RandomForest:
        - Accuracy: 87.77 ± 1.78
        - Sentivity: 87.74 ± 3.18
        - Specificity: 88.49 ± 3.17
        - MCC: 75.45 ± 3.83
        - AUC (ROC): 93.47 ± 2.83
    - Tiempo de CPU en terminar las 5 corridas: 129.26 segundos 


**Resultado de Features seleccionados por el paper:**

![Resultado de los mejores Features obtenidos por el paper](BestResult.png)

## Pasos

1. Implementar el algoritmo Binary Black Hole. **OK**
    1. Los experimentos serán los mismos que en el primer paper: utilizando Random Forest y midiendo las métricas. **OK**
1. Ver si se puede mejorar a partir del segundo paper. **OK**
1. Correr experimentos en el algoritmo Spark:
    1. Usando F1-Score con todos los features
    1. Usando F1-Score con el top 50 de los features
1. Correr experimento con todos los features sin Spark (solo tenemos medidos los tiempos para el top 50).
1. Probar de correr los experimentos con el SVM en Spark (tanto con todos los features como con solo el top 50) para ver si sirve dicho algoritmo con tan pocos ejemplos (algo característico de estos datasets).
1. Incorporar el dataset que hizo Mati Butti (lo tengo en un mail).
1. Comparar tiempos y escribir el paper.
1. Hacer un refactoring del código ya que lo único que cambia es la función de fitness.


## Datasets

Los datasets son sacados de [acá](http://csse.szu.edu.cn/staff/zhuzx/Datasets.html). Los archivos están en formato ARFF, y se dejan en la carpeta _Datasets_. Se toman únicamente los de cáncer de colon (Colon.arff), mama (Breast.arff), leucemia (Leukemia.arff) y ovario (Ovarian) para reducir el tamaño del problema (por lo menos, en su primera etapa). **Hay algunos datasets que se mencionan en el paper, pero que no están para descargar en la página de datasets.


## Consideraciones

Hay que citar el paper _Markov Blanket-Embedded Genetic Algorithm for Gene Selection_ si se usan los datasets