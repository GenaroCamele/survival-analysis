from typing import Tuple, Any
from pandas import Series
from pyspark.ml.classification import RandomForestClassifier
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.ml.linalg import Vectors, DenseVector, VectorUDT
from pyspark.sql import SparkSession, DataFrame as SparkDataFrame
from pyspark.ml.tuning import ParamGridBuilder, CrossValidator, CrossValidatorModel
from pyspark import SparkConf, SparkContext
from pyspark.sql.types import StructField, StructType, FloatType
from core import run_experiment
from paper_util import get_columns_from_df, NEW_CLASS_NAME
import time
import logging

# Habilito los loggers
logging.getLogger().setLevel(logging.INFO)

# Cantidad de procesadores de la maquina utilizados para el Cross Validation. -1 = usar todos
N_JOBS = -1

# Configuracion de Spark
# Enable Arrow-based columnar data transfers
conf = SparkConf().setMaster("local").setAppName("WordCount").set("'spark.sql.execution.arrow.pyspark.enabled", "true")
sc = SparkContext(conf=conf)
sc.setLogLevel("ERROR")
sql = SparkSession(sc)


def generate_labeled_point_from_row(row: Series) -> Tuple[float, DenseVector]:
    """
    Genera un LabeledPoint a partir de una Row para poder entrenar un SVM
    :param row: Row a parsear
    :return: LabeledPoint
    """
    return row.loc[NEW_CLASS_NAME], Vectors.dense(row.drop(NEW_CLASS_NAME))


def compute_cross_validation(parsed_data: SparkDataFrame, _y) -> float:
    """
    Computa una validacion cruzada calculando el F1-Socre
    :param parsed_data: DataFrame with parsed data
    :param _y: Unused parameter for compatibility with sequential schema
    :return: Promedio del F1-Socre obtenido en cada fold del CrossValidation
    """
    # Entreno SVM con Gradiente Descendente Estocastico
    crossval = CrossValidator(estimator=RandomForestClassifier(numTrees=500),
                              estimatorParamMaps=ParamGridBuilder().build(),
                              evaluator=MulticlassClassificationEvaluator(metricName='f1'),
                              numFolds=10,
                              parallelism=4)

    start = time.time()
    cross_model: CrossValidatorModel = crossval.fit(parsed_data)
    logging.info(f'Tiempo de cross validation -> {time.time() - start} segundos')

    f1_score = cross_model.avgMetrics[0]
    return f1_score


def get_parsed_data(subset, x, y) -> Tuple[SparkDataFrame, Any]:
    """
    Obtiene la data parseada. Sirve para no procesar varias veces lo mismo
    :param subset: Subset de features a utilizar en el RandomForest evaluado en el CrossValidation
    :param x: Datos de los features
    :param y: Clases
    :return: Spark SQL DataFrame listo para el CrossValidation
    """
    # start = time.time()
    subset_x = get_columns_from_df(subset, x)
    subset_x[NEW_CLASS_NAME] = y

    def parse_row(idx_and_row):
        (_, row) = idx_and_row
        return row.loc[NEW_CLASS_NAME].item(), Vectors.dense(row.drop(NEW_CLASS_NAME))

    # start = time.time()
    transformed = list(map(parse_row, subset_x.iterrows()))

    # start = time.time()
    parsed_data = sql.createDataFrame(transformed, schema=StructType([
        StructField("label", FloatType()),
        StructField("features", VectorUDT())
    ]))

    # IMPORTANT: devuelve None para compatibilidad con el esquema secuencial
    return parsed_data.cache(), None


def main():
    run_experiment(
        datasets=[
            {'file_name': 'Ovarian', 'class_col_name': 'Class'},
            {'file_name': 'Leukemia', 'class_col_name': 'CLASS'},
            {'file_name': 'Breast', 'class_col_name': 'Class'}
        ],
        get_parsed_data=get_parsed_data,
        compute_cross_validation=compute_cross_validation,
        metric_description='f1-score'
    )


if __name__ == '__main__':
    main()
