# En este archivo se encuentran todas las funciones para corroborar las metricas obtenidas por la metaheuristica
from typing import Dict, Callable, cast
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import make_scorer, matthews_corrcoef
from sklearn.model_selection import cross_validate
from paper_util import specificity

# Cantidad de procesadores de la maquina utilizados para el Cross Validation. -1 = usar todos
N_JOBS = -1

# Diccionario con la configuracion de Scoring para abstraernos de la implementacion ya que hay tipos mixtos
SCORING_DICT = {
    'f1': 'f1',
    'accuracy': 'accuracy',
    'roc_auc': 'roc_auc',
    'matthews_corrcoef (MCC)': make_scorer(matthews_corrcoef),
    'sensitivity': 'recall',
    'specificity': make_scorer(specificity, greater_is_better=True)
}


def get_dict_of_metrics_clean() -> Dict:
    """
    Genera un diccionario con las metricas mencionadas en el paper como clave y una lista vacia como valor
    para ir almacenando los promedios obtenidos para cada metrica
    :return: Diccionario con las caracteristicas especificadas
    """
    res = SCORING_DICT.copy()
    for key in res.keys():
        res[key] = []
    return res


def get_metrics_mean(all_metrics: Dict) -> Dict:
    """
    Toma un diccionario y genera un nuevo diccionario cuyas claves son las metricas mencionadas en el paper
    y el valor es el promedio de los valores de la lista que se tenia por cada clave originalmente
    :param all_metrics: Diccionario con un arreglo de valores para cada una de las claves (metricas)
    :return: Diccionario con el promedio de los valores para cada clave (metrica)
    """
    res = {}
    for key in all_metrics.keys():
        # Elimina el prefijo 'test_' de la clave, que es lo que le pone automaticamente
        # Sklearn cuando calcula el scoring en el CrossValidation
        new_key = key.split('test_', 1)[-1]
        res[new_key] = all_metrics[key].mean()
    return res


def merge_dicts(dict_1: Dict, dict_2: Dict):
    """
    Hace un append de los valores que contienen ambos diccionarios pasados por parametros y se almacenan en el
    primer diccionario
    :param dict_1: Primer diccionario donde se almacenaran los resultados
    :param dict_2: Segundo diccionario
    """
    for key in dict_1.keys():
        appended = np.append(dict_1[key], dict_2[key])
        dict_1[key] = appended


def get_all_metrics_from_cross_val(subset_x: pd.DataFrame, y: np.ndarray) -> Dict:
    """
    Computa una validacion cruzada calculando todas las metricas mencionadas en el paper
    :param subset_x: Subset de features a utilizar en el RandomForest evaluado en el CrossValidation
    :param y: Clases
    :return: Diccionario con las metricas como claves y los valores de cada una de las metricas obtenidas en cada fold
    """

    # NOTA: se usa RandomForest porque es el modelo que mejor les funcionó en el paper
    # IMPORTANTE: ellos usan el package randomForest de R, en dicha libreria el default para el nro de árboles es 500
    return cross_validate(
        RandomForestClassifier(n_estimators=500),
        subset_x,
        y,
        cv=10,
        scoring=SCORING_DICT,
        n_jobs=N_JOBS
    )


def get_mean_100_runs(subset_x: pd.DataFrame, y: np.ndarray) -> Dict:
    """
    Realiza 100 corridas de un CrossValidation que obtiene todas las metricas del paper
    :param subset_x: Datos de X que contiene solo los features seleccionados
    :param y: Array de las clases
    :return: Diccionario con el promedio de las 5 métricas para las 100 corridas
    """
    # Saco el promedio de 100 corridas con el mejor subset obtenido
    result_100_runs = get_dict_of_metrics_clean()
    for _ in range(100):
        all_metrics = get_all_metrics_from_cross_val(subset_x, y)
        all_metrics_mean = get_metrics_mean(all_metrics)
        merge_dicts(result_100_runs, all_metrics_mean)
    result_100_runs_mean = get_metrics_mean(result_100_runs)
    return result_100_runs_mean


def get_mean_n_runs(number_of_runs: int, f1_fitness_function: Callable) -> float:
    """
    Realiza 100 corridas de un CrossValidation que obtiene todas las metricas del paper
    :param number_of_runs: Numero de corridas a ejecutar
    :param f1_fitness_function: Funcion que devuelve el F1-Score
    :return: Diccionario con el promedio de las 100 corridas
    """
    # Saco el promedio de 100 corridas con el mejor subset obtenido
    result_100_runs = []
    for _ in range(number_of_runs):
        result_100_runs.append(f1_fitness_function())
    return cast(float, np.mean(result_100_runs))
