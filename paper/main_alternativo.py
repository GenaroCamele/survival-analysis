from typing import List
import pandas as pd
from sklearn.feature_selection import chi2
from sklearn.model_selection import cross_val_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import MinMaxScaler
import numpy as np
from paper.custom_metaheuristics import binary_black_hole, improved_binary_black_hole
from paper.paper_util import specificity, get_x_and_y, read_data
from util import get_columns_from_df
from sklearn.metrics import make_scorer, matthews_corrcoef
import time

# Cantidad de procesadores de la maquina utilizados para el Cross Validation. -1 = usar todos
N_JOBS = -1

# Diccionario con la configuracion de Scoring para abstraernos de la implementacion ya que hay tipos mixtos
SCORING_DICT = {
    'accuracy': 'accuracy',
    'roc_auc': 'roc_auc',
    'matthews_corrcoef (MCC)': make_scorer(matthews_corrcoef),
    'sensitivity': 'recall',
    'specificity': make_scorer(specificity, greater_is_better=True)
}


def filter_by_chi_squared(x: pd.DataFrame, y: np.ndarray) -> List[str]:
    """
    Según el paper, se realiza un filtrado previo dejando el top de 50 features mas relevantes
    segun el test estadístico Chi Square.
    Ver -> https://towardsdatascience.com/chi-square-test-for-feature-selection-in-machine-learning-206b1f0b8223
    :param x: DataFrame con los features
    :param y: Labels
    :return: Top 50 de features mas significativos
    """
    # TODO: chequear si esta bien. chi2 no acepta numero negativos asi que estoy normalizando [0-1]
    # TODO: ver si no se deberia normalizar [0-1] pero considerando toda la matriz en vez de por columnas
    # Escala entre [0-1]
    x_scaled = MinMaxScaler().fit_transform(x)
    scaled_df = pd.DataFrame(x_scaled, columns=x.columns, index=x.index)

    # Computa el Chi-Square
    chi_scores = chi2(scaled_df, y)

    # Ordena por p-valor y devuelve los primeros 50
    p_values = pd.Series(chi_scores[1], index=x.columns)
    p_values.sort_values(ascending=True, inplace=True)
    return p_values.index.values[:50]


def compute_cross_validation(subset: np.array, x: pd.DataFrame, y: np.ndarray, metric) -> float:
    subset_x = get_columns_from_df(subset, x)

    # NOTA: se usa RandomForest porque es el modelo que mejor les funcionó en el paper
    # IMPORTANTE: ellos usan el package randomForest de R, en dicha libreria el default para el nro de árboles es 500
    return cross_val_score(
        RandomForestClassifier(n_estimators=500),
        subset_x,
        y,
        cv=10,
        scoring=metric,
        n_jobs=N_JOBS
    ).mean()


def binary_black_hole_fitness_function(index_array: np.array, x: pd.DataFrame, y: np.ndarray, metric) -> float:
    if not np.count_nonzero(index_array):
        return -1.0
    return compute_cross_validation(index_array, x, y, metric)


def main():
    # Array para guardar en formato CSV a lo ultimo
    all_experiments_results = []
    for cancer_dataset, dataset_name in read_data():
        # Separa los datos para obtener X e Y
        x, y, number_classes = get_x_and_y(cancer_dataset)

        number_samples, number_features = x.shape

        print(f'Dataset {dataset_name}')
        print(f'\tSamples (filas) -> {number_samples} | Features (columnas) -> {number_features}')
        print(f'\tNumero de clases -> {number_classes}')

        # Hace primero un Feature Selection filtrando el top por Chi Square Test
        top_features = filter_by_chi_squared(x, y)
        x = x[top_features]

        # Imprimo para probar
        print(f'Top 50 Features obtenidos por Chi Square -> ')
        print(top_features)

        # Needed parameter for the Binary Black Hole Algorithm
        n_features = x.shape[1]

        # TODO: borrar este for, es solo para dejar corriendo toda la noche
        for run_improved in [False, True]:
            for metric_id in ['sensitivity', 'matthews_corrcoef (MCC)', 'specificity', 'accuracy', 'roc_auc']:
                metric = SCORING_DICT[metric_id]

                # Array de metricas para sacar el promedio y la deviacion estandar
                independent_start_time = time.time()
                independent_results = []
                final_subset = None  # Mejor subset obtenido
                best_metric = -1  # Mejor metrica obtenida
                for i in range(5):
                    # Binary Black Hole
                    if run_improved:
                        best_subset, best_fitness = improved_binary_black_hole(
                            n_stars=10,
                            n_features=n_features,
                            n_iterations=25,
                            fitness_function=lambda subset: binary_black_hole_fitness_function(subset, x, y, metric),
                            coeff_1=2.35,
                            coeff_2=0.2,
                            binary_threshold=None  # Usamos threshold aleatorio cada pasada (otro valor recomendado es 0.6)
                        )
                    else:
                        best_subset, best_fitness = binary_black_hole(
                            n_stars=10,
                            n_features=n_features,
                            n_iterations=25,
                            fitness_function=lambda subset: binary_black_hole_fitness_function(subset, x, y, metric),
                            binary_threshold=None  # Usamos threshold aleatorio cada pasada (otro valor recomendado es 0.6)
                        )

                    # Obtengo el nombre de las columnas
                    column_names = get_columns_from_df(best_subset, x).columns.values

                    # Saco el promedio de 100 corridas con el mejor subset obtenido
                    result_100_runs = []
                    for j in range(100):
                        result_100_runs.append(binary_black_hole_fitness_function(best_subset, x, y, metric))

                    current_metric_mean = np.mean(result_100_runs)

                    # Informo metricas de las 100 corridas
                    print(f'Iteracion -> {i + 1} despues de 100 corridas con el subset se obtuvo de "{metric_id}" '
                          f'-> {current_metric_mean} ± {np.std(result_100_runs)}')

                    independent_results.append(current_metric_mean)
                    if current_metric_mean > best_metric:
                        best_metric = current_metric_mean
                        final_subset = column_names

                    # Imprime informacion del resultado
                    # print(f'Resultados para {field_name}')
                    # print(f'\tLa mejor combinación -> {column_names}')
                    # print(f'\tBest Cross V. -> {best_fitness} (métrica "{metric}")')
                    # print(f'\tReducido de {n_features} a {len(column_names)}')

                # Informo resultados finales
                independent_mean = round(np.mean(independent_results), 3)
                independent_std = round(np.std(independent_results), 3)
                independent_run_time = round(time.time() - independent_start_time, 3)

                # Prints para debug
                algorithm = 'BBHA' + (' (improved)' if run_improved else '')
                print(f'Features obtenidos por el {algorithm} para el dataset {dataset_name} ({metric_id} = {best_metric}) ->')
                print(final_subset)
                print(f'Metrica "{metric_id}" | Mean -> {independent_mean} ± {independent_std}')
                print(f'5 corridas terminadas en {independent_run_time} segundos')

                all_experiments_results.append({
                    'dataset': dataset_name,
                    'BBHA mejorado': 1 if run_improved else 0,
                    'Metrica descripcion': metric_id,
                    'Mejor metrica en 5 corridas': best_metric,
                    'Metrica promedio de 5 corridas': f'{independent_mean} ± {independent_std}',
                    'Features finales': ' | '.join(final_subset),
                    'Tiempo de CPU (5 corridas) en segundos': independent_run_time,
                })

    # Guardo a CSV todos los resultados obtenidos
    now = time.strftime('%Y-%m-%d_%H:%M:%S')
    pd.DataFrame(all_experiments_results).to_csv(f'./Resultados/result_{now}.csv')


if __name__ == '__main__':
    main()
