docker run -it \
 -d \
 --name spark_submit_sequential \
 --network spark_cluster_spark-net \
 -p 4040:4040 \
 -v /home/labmovil1/ClusterSpark/scripts:/home/scripts \
 midusi/spark_submit bash -c "python3 ./scripts/survival-analysis/paper/sequential.py"
