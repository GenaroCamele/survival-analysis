docker run -it \
 -d \
 --name spark_submit_bbha_spark_version \
 --network spark_cluster_spark-net \
 -p 4040:4040 \
 -v /home/labmovil1/ClusterSpark/scripts:/home/scripts \
 midusi/spark_submit bash -c "./scripts/submit-script.sh ./scripts/survival-analysis/paper/spark.py"
