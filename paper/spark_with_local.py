from typing import Tuple
import pandas as pd
from pandas import Series
from pyspark.ml.classification import RandomForestClassifier
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.ml.linalg import Vectors, DenseVector
from pyspark.sql import SparkSession, DataFrame as SparkDataFrame
from pyspark.ml.tuning import ParamGridBuilder, CrossValidator, CrossValidatorModel
from pyspark import SparkConf, SparkContext
import numpy as np
from custom_metaheuristics import binary_black_hole, improved_binary_black_hole
from metrics import get_mean_n_runs
from paper_util import get_x_and_y, read_data, get_columns_from_df, NEW_CLASS_NAME
import time

# Cantidad de procesadores de la maquina utilizados para el Cross Validation. -1 = usar todos
N_JOBS = -1

# Parametros del Binary Black Hole
N_STARS = 10
# N_STARS = 2  # Para pruebas
N_ITERATIONS = 25
# N_ITERATIONS = 1  # Para pruebas

# Estos 2 de abajo son solo para la version alternativa de BBH
COEFF_1 = 2.35
COEFF_2 = 0.2

# Configuracion de Spark
# Enable Arrow-based columnar data transfers
conf = SparkConf().setMaster("local").setAppName("WordCount").set("'spark.sql.execution.arrow.pyspark.enabled", "true")
sc = SparkContext(conf=conf)
sql = SparkSession(sc)

# Instacio el logger
log4jLogger = sc._jvm.org.apache.log4j
LOGGER = log4jLogger.LogManager.getLogger(__name__)


def generate_labeled_point_from_row(row: Series) -> Tuple[float, DenseVector]:
    """
    Genera un LabeledPoint a partir de una Row para poder entrenar un SVM
    :param row: Row a parsear
    :return: LabeledPoint
    """
    return row.loc[NEW_CLASS_NAME], Vectors.dense(row.drop(NEW_CLASS_NAME))


def compute_cross_validation(parsed_data: SparkDataFrame) -> float:
    """
    Computa una validacion cruzada calculando el F1-Socre
    :return: Promedio del F1-Socre obtenido en cada fold del CrossValidation
    """
    # Parametros a probar en el CrossValidation los dejo vacios, pero se podría probar distintos parámetros
    # del RandomForest
    param_grid = ParamGridBuilder().build()

    # Entreno SVM con Gradiente Descendente Estocastico
    crossval = CrossValidator(estimator=RandomForestClassifier(numTrees=500),
                              estimatorParamMaps=param_grid,
                              evaluator=MulticlassClassificationEvaluator(metricName='f1'),
                              numFolds=10)

    start = time.time()
    cross_model: CrossValidatorModel = crossval.fit(parsed_data)
    LOGGER.info(f'Tiempo de cross validation -> {time.time() - start} segundos')

    f1_score = cross_model.avgMetrics[0]
    return f1_score


def get_parsed_data(subset, x, y) -> SparkDataFrame:
    """
    Obtiene la data parseada. Sirve para no procesar varias veces lo mismo
    :param subset: Subset de features a utilizar en el RandomForest evaluado en el CrossValidation
    :param x: Datos de los features
    :param y: Clases
    :return: Spark SQL DataFrame listo para el CrossValidation
    """
    subset_x = get_columns_from_df(subset, x)
    start = time.time()
    subset_x[NEW_CLASS_NAME] = y
    LOGGER.info(f'Tiempo de la concatenacion -> {time.time() - start} segundos')

    start = time.time()
    transformed = subset_x.apply(generate_labeled_point_from_row, axis=1, result_type='expand')
    LOGGER.info(f'Tiempo de transformacion -> {time.time() - start} segundos')

    start = time.time()
    parsed_data = sql.createDataFrame(transformed, ['label', 'features'])
    LOGGER.info(f'Tiempo de creacion de dataframe de Spark -> {time.time() - start} segundos')

    return parsed_data


def binary_black_hole_fitness_function(index_array: np.array, x: pd.DataFrame, y: np.ndarray) -> float:
    """
    Funcion de fitness de una estrella evaluada en el Binary Black hole
    :param index_array: Lista de booleanos indicando cual feature debe ser incluido en la evaluacion y cual no
    :param x: Datos de los features
    :param y: Clases
    :return: Promedio del F1-Socre obtenido en cada fold del CrossValidation. -1 si no hay features a evaluar
    """
    if not np.count_nonzero(index_array):
        return -1.0

    parsed_data = get_parsed_data(index_array, x, y)
    return compute_cross_validation(parsed_data)


def main():
    # Array para guardar en formato CSV a lo ultimo
    all_experiments_results = []
    for cancer_dataset, dataset_name in read_data():
        # Separa los datos para obtener X e Y
        x, y, number_classes = get_x_and_y(cancer_dataset)

        number_samples, number_features = x.shape

        LOGGER.info(f'Dataset {dataset_name}')
        LOGGER.info(f'\tSamples (filas) -> {number_samples} | Features (columnas) -> {number_features}')
        LOGGER.info(f'\tNumero de clases -> {number_classes}')

        # Needed parameter for the Binary Black Hole Algorithm
        n_features = x.shape[1]

        for run_improved in [False, True]:
            independent_start_time = time.time()
            # independent_results: Dict[str, list] = get_dict_of_metrics_clean()
            final_subset = None  # Mejor subset obtenido
            best_metric = -1  # Mejor metrica obtenida
            for i in range(5):
                # Binary Black Hole
                if run_improved:
                    best_subset, best_fitness = improved_binary_black_hole(
                        n_stars=N_STARS,
                        n_features=n_features,
                        n_iterations=N_ITERATIONS,
                        fitness_function=lambda subset: binary_black_hole_fitness_function(subset, x, y),
                        coeff_1=COEFF_1,
                        coeff_2=COEFF_2,
                        binary_threshold=None  # Usamos threshold aleatorio cada pasada (otro valor recomendado es 0.6)
                    )
                else:
                    best_subset, best_fitness = binary_black_hole(
                        n_stars=N_STARS,
                        n_features=n_features,
                        n_iterations=N_ITERATIONS,
                        fitness_function=lambda subset: binary_black_hole_fitness_function(subset, x, y),
                        binary_threshold=None  # Usamos threshold aleatorio cada pasada (otro valor recomendado es 0.6)
                    )

                parsed_data = get_parsed_data(best_subset, x, y)
                current_f1_score = get_mean_n_runs(
                    100,
                    lambda: compute_cross_validation(parsed_data)
                )

                # Informo metricas de las 100 corridas
                # print(f'Iteracion {i + 1}, 100 corridas terminadas en {time.time() - iterations_100_start} segundos')

                # Vemos si es la mejor metrica
                if current_f1_score > best_metric:
                    best_metric = current_f1_score

                    # Obtengo el nombre de las columnas
                    column_names = get_columns_from_df(best_subset, x).columns.values
                    final_subset = column_names

                    # Imprime informacion del resultado
                    # print(f'Resultados para {field_name}')
                    # print(f'\tLa mejor combinación -> {column_names}')
                    # print(f'\tBest Cross V. -> {best_fitness} (métrica "{metric}")')
                    # print(f'\tReducido de {n_features} a {len(column_names)}')

            # Informo resultados finales
            independent_run_time = round(time.time() - independent_start_time, 3)
            LOGGER.info(f'Tiempo de 5 corridas -> {independent_run_time} segundos')

            experiment_results_dict = {
                'dataset': dataset_name,
                'BBHA mejorado': 1 if run_improved else 0,
                'Mejor F1-Score (en 5 corridas)': round(best_metric, 4),
                'Features con mejor F1-Score (en 5 corridas)': ' | '.join(final_subset),
                'Tiempo de CPU (5 corridas) en segundos': independent_run_time,
            }

            # for metric, value in independent_results.items():
            #     independent_mean = round(np.mean(value), 4)
            #     independent_std = round(np.std(value), 4)
            #     csv_result_key = f'{metric} promedio de 5 corridas'
            #     experiment_results_dict[csv_result_key] = f'{independent_mean} (± {independent_std})'
                # print(f'Metrica {metric} -> {independent_mean} (± {independent_std})')

            # Prints para debug
            # algorithm = 'BBHA' + (' (improved)' if run_improved else '')
            # print(f'Features con {algorithm} para el dataset {dataset_name} (F1-Score = {best_metric}) ->')
            # print(final_subset)

            all_experiments_results.append(experiment_results_dict)

    # Guardo a CSV todos los resultados obtenidos
    now = time.strftime('%Y-%m-%d_%H:%M:%S')
    destination_path = f'./Resultados/result_{now}.csv'
    pd.DataFrame(all_experiments_results).to_csv(destination_path)
    print(f'Result saved in {destination_path}')


if __name__ == '__main__':
    main()
