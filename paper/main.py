from typing import List, Dict
import pandas as pd
from sklearn.feature_selection import chi2
from sklearn.model_selection import cross_val_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import MinMaxScaler
import numpy as np
from custom_metaheuristics import binary_black_hole, improved_binary_black_hole
from metrics import get_dict_of_metrics_clean, merge_dicts, get_mean_100_runs
from paper_util import get_x_and_y, read_data, get_columns_from_df
import time

# Cantidad de procesadores de la maquina utilizados para el Cross Validation. -1 = usar todos
N_JOBS = -1


def filter_by_chi_squared(x: pd.DataFrame, y: np.ndarray) -> List[str]:
    """
    Según el paper, se realiza un filtrado previo dejando el top de 50 features mas relevantes
    segun el test estadístico Chi Square.
    Ver -> https://towardsdatascience.com/chi-square-test-for-feature-selection-in-machine-learning-206b1f0b8223
    :param x: DataFrame con los features
    :param y: Labels
    :return: Top 50 de features mas significativos
    """
    # TODO: chequear si esta bien. chi2 no acepta numero negativos asi que estoy normalizando [0-1]
    # TODO: ver si no se deberia normalizar [0-1] pero considerando toda la matriz en vez de por columnas
    # Escala entre [0-1]
    x_scaled = MinMaxScaler().fit_transform(x)
    scaled_df = pd.DataFrame(x_scaled, columns=x.columns, index=x.index)

    # Computa el Chi-Square
    chi_scores = chi2(scaled_df, y)

    # Ordena por p-valor y devuelve los primeros 50
    p_values = pd.Series(chi_scores[1], index=x.columns)
    p_values.sort_values(ascending=True, inplace=True)
    return p_values.index.values[:50]


def compute_cross_validation(subset: np.array, x: pd.DataFrame, y: np.ndarray) -> float:
    """
    Computa una validacion cruzada calculando el accuracy
    :param subset: Subset de features a utilizar en el RandomForest evaluado en el CrossValidation
    :param x: Datos de los features
    :param y: Clases
    :return: Promedio del accuracy obtenido en cada fold del CrossValidation
    """
    subset_x = get_columns_from_df(subset, x)

    # NOTA: se usa RandomForest porque es el modelo que mejor les funcionó en el paper
    # IMPORTANTE: ellos usan el package randomForest de R, en dicha libreria el default para el nro de árboles es 500
    return cross_val_score(
        RandomForestClassifier(n_estimators=500),
        subset_x,
        y,
        cv=10,
        scoring='accuracy',
        n_jobs=N_JOBS
    ).mean()


def binary_black_hole_fitness_function(index_array: np.array, x: pd.DataFrame, y: np.ndarray) -> float:
    """
    Funcion de fitness de una estrella evaluada en el Binary Black hole
    :param index_array: Lista de booleanos indicando cual feature debe ser incluido en la evaluacion y cual no
    :param x: Datos de los features
    :param y: Clases
    :return: Promedio del accuracy obtenido en cada fold del CrossValidation. -1 si no hay features a evaluar
    """
    if not np.count_nonzero(index_array):
        return -1.0
    return compute_cross_validation(index_array, x, y)


def main():
    # Array para guardar en formato CSV a lo ultimo
    all_experiments_results = []
    for cancer_dataset, dataset_name in read_data():
        # Separa los datos para obtener X e Y
        x, y, number_classes = get_x_and_y(cancer_dataset)

        number_samples, number_features = x.shape

        print(f'Dataset {dataset_name}')
        print(f'\tSamples (filas) -> {number_samples} | Features (columnas) -> {number_features}')
        print(f'\tNumero de clases -> {number_classes}')

        # Hace primero un Feature Selection filtrando el top por Chi Square Test
        top_features = filter_by_chi_squared(x, y)
        x = x[top_features]

        # Imprimo para probar
        print(f'Top 50 Features obtenidos por Chi Square -> ')
        print(top_features)

        # Needed parameter for the Binary Black Hole Algorithm
        n_features = x.shape[1]

        for run_improved in [False, True]:
            independent_start_time = time.time()
            independent_results: Dict[str, list] = get_dict_of_metrics_clean()
            final_subset = None  # Mejor subset obtenido
            best_metric = -1  # Mejor metrica obtenida
            for i in range(5):
                # Binary Black Hole
                if run_improved:
                    best_subset, best_fitness = improved_binary_black_hole(
                        n_stars=10,
                        n_features=n_features,
                        n_iterations=25,
                        fitness_function=lambda subset: binary_black_hole_fitness_function(subset, x, y),
                        coeff_1=2.35,
                        coeff_2=0.2,
                        binary_threshold=None  # Usamos threshold aleatorio cada pasada (otro valor recomendado es 0.6)
                    )
                else:
                    best_subset, best_fitness = binary_black_hole(
                        n_stars=10,
                        n_features=n_features,
                        n_iterations=25,
                        fitness_function=lambda subset: binary_black_hole_fitness_function(subset, x, y),
                        binary_threshold=None  # Usamos threshold aleatorio cada pasada (otro valor recomendado es 0.6)
                    )

                subset_x = get_columns_from_df(best_subset, x)
                result_100_runs_mean = get_mean_100_runs(subset_x, y)

                # Informo metricas de las 100 corridas
                # print(f'Iteracion {i + 1}, 100 corridas terminadas en {time.time() - iterations_100_start} segundos')

                # Obtengo el nombre de las columnas
                column_names = get_columns_from_df(best_subset, x).columns.values

                merge_dicts(independent_results, result_100_runs_mean)
                # NOTA: solo se considera el accuracy
                current_accuracy = result_100_runs_mean['accuracy']
                if current_accuracy > best_metric:
                    best_metric = current_accuracy
                    final_subset = column_names

                # Imprime informacion del resultado
                # print(f'Resultados para {field_name}')
                # print(f'\tLa mejor combinación -> {column_names}')
                # print(f'\tBest Cross V. -> {best_fitness} (métrica "{metric}")')
                # print(f'\tReducido de {n_features} a {len(column_names)}')

            # Informo resultados finales
            independent_run_time = round(time.time() - independent_start_time, 3)
            # print(f'5 corridas terminadas en {independent_run_time} segundos')

            experiment_results_dict = {
                'dataset': dataset_name,
                'BBHA mejorado': 1 if run_improved else 0,
                'Mejor accuracy (en 5 corridas)': round(best_metric, 4),
                'Features con mejor accuracy (en 5 corridas)': ' | '.join(final_subset),
                'Tiempo de CPU (5 corridas) en segundos': independent_run_time,
            }

            for metric, value in independent_results.items():
                independent_mean = round(np.mean(value), 4)
                independent_std = round(np.std(value), 4)
                csv_result_key = f'{metric} promedio de 5 corridas'
                experiment_results_dict[csv_result_key] = f'{independent_mean} (± {independent_std})'
                # print(f'Metrica {metric} -> {independent_mean} (± {independent_std})')

            # Prints para debug
            # algorithm = 'BBHA' + (' (improved)' if run_improved else '')
            # print(f'Features con {algorithm} para el dataset {dataset_name} (accuracy = {best_metric}) ->')
            # print(final_subset)

            all_experiments_results.append(experiment_results_dict)
    # Guardo a CSV todos los resultados obtenidos
    now = time.strftime('%Y-%m-%d_%H:%M:%S')
    pd.DataFrame(all_experiments_results).to_csv(f'./Resultados/result_{now}.csv')


if __name__ == '__main__':
    main()
