from sklearn.model_selection import cross_val_score
from sklearn.ensemble import RandomForestClassifier
import numpy as np
from core import run_experiment
from paper_util import get_columns_from_df
import logging
import time

# Habilito los loggers
logging.getLogger().setLevel(logging.INFO)

# Cantidad de procesadores de la maquina utilizados para el Cross Validation. -1 = usar todos
N_JOBS = -1


def compute_cross_validation(subset: np.array, y: np.ndarray) -> float:
    """
    Computa una validacion cruzada calculando el accuracy
    :param subset: Subset de features a utilizar en el RandomForest evaluado en el CrossValidation
    :param y: Clases
    :return: Promedio del accuracy obtenido en cada fold del CrossValidation
    """
    # NOTA: se usa RandomForest porque es el modelo que mejor les funcionó en el paper
    # IMPORTANTE: ellos usan el package randomForest de R, en dicha libreria el default para el nro de árboles es 500
    start = time.time()
    res = cross_val_score(
        RandomForestClassifier(n_estimators=500),
        subset,
        y,
        cv=10,
        scoring='accuracy',
        n_jobs=N_JOBS
    )
    logging.info(f'Tiempo de cross validation -> {time.time() - start} segundos')

    return res.mean()


def get_parsed_data(index_array, x, y):
    return get_columns_from_df(index_array, x), y


def main():
    run_experiment(
        datasets=[
            {'file_name': 'Ovarian', 'class_col_name': 'Class'},
            {'file_name': 'Leukemia', 'class_col_name': 'CLASS'},
            {'file_name': 'Breast', 'class_col_name': 'Class'}
        ],
        get_parsed_data=get_parsed_data,
        compute_cross_validation=compute_cross_validation,
        metric_description='accuracy'
    )


if __name__ == '__main__':
    main()
