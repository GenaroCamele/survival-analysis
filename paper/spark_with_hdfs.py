import time
from typing import Tuple, Dict
from custom_metaheuristics import binary_black_hole, improved_binary_black_hole
import numpy as np
from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession
from pyspark.sql import DataFrame
from pyspark.sql.types import Row
from pyspark.ml.feature import StringIndexer
from pyspark.ml.linalg import Vectors, DenseVector
from pyspark.ml.tuning import CrossValidator, ParamGridBuilder, CrossValidatorModel
from pyspark.ml.classification import RandomForestClassifier
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
import pandas as pd
from metrics import get_dict_of_metrics_clean, get_mean_100_runs, merge_dicts

CLASS_NAME = 'class'
CON_PIPELINE = True

# Parametros del Binary Black Hole
N_STARS = 10
N_ITERATIONS = 25

# Estos 2 de abajo son solo para la version alternativa de BBH
COEFF_1 = 2.35
COEFF_2 = 0.2


def binarize_y(df: DataFrame) -> DataFrame:
    """
    Toma un DataFrame y transforma la columna 'class' que es un string a binario
    :param df: DataFrame a convertir
    :return: DataFrame convertido
    """
    new_class_name = f'{CLASS_NAME}_new'
    string_indexer = StringIndexer(inputCol=CLASS_NAME, outputCol=new_class_name)
    new_df = string_indexer.fit(df).transform(df)
    return new_df.drop(CLASS_NAME).withColumnRenamed(new_class_name, CLASS_NAME)


def fix_column(col: str) -> str:
    """
    Limpia en nombre de la columna pasada por parametro eliminando caracteres como '.' o '-'
    que pueden provocar errores al hacer un select() en el DataFrame
    :param col: Nombre de la columna a limpiar
    :return: Nombre de la columna limpia
    """
    new_name = col.strip()
    return "".join(new_name.split())\
        .replace('.', '_')\
        .replace('-', '_')


def fix_columns_names(df: DataFrame) -> DataFrame:
    """
    Limpia los nombre de las columnas del DataFrame eliminando caracteres como '.' o '-'
    que pueden provocar errores al hacer un select() en el DataFrame
    :param df: DataFrame a limpiar sus columnas
    :return: DataFrame con las columnas limpias
    """
    old_columns = df.schema.names
    new_columns = list(map(fix_column, old_columns))
    return df.toDF(*new_columns)


def get_columns_by_indexes(subset_indexes: np.ndarray, columns_names: np.ndarray):
    """
    Obtiene los nombres de las columnas (strings) a partir de un arreglos de binarios donde 0 significa que
    la columna no debe retornarse, y 1 la columna deber ser incluida
    :param subset_indexes: Array binario para saber que columnas incluir
    :param columns_names: Arreglo con todas las columnas totales
    :return: Arreglo con solo los nommbres de las columnas cuya posicion eran 1 en subset_indexes
    """
    non_zero_idx = np.nonzero(subset_indexes)
    return columns_names[non_zero_idx]


def generate_labeled_point_from_row(row: Row) -> Tuple[float, DenseVector]:
    """
    Genera un LabeledPoint a partir de una Row para poder entrenar un SVM
    :param row: Row a parsear
    :return: LabeledPoint
    """
    return row[-1], Vectors.dense(row[:-1])


def get_parsed_data(index_array: np.array, columns_names: np.ndarray, df: DataFrame) -> DataFrame:
    # Obtengo los nombres de las columas a extraer del DataFrame
    subset_columns_names = get_columns_by_indexes(index_array, columns_names)

    # Se le agrega la columna de label 'class'
    subset_columns_names: np.ndarray = np.append(subset_columns_names, [CLASS_NAME])
    subset = df.select(*subset_columns_names)

    # IMPORTANTE: implementa a partir de un mapeo de RDDs que después vuelven a convertirse en DataFrames
    # Genero por cada fila un LabeledPoint
    return subset.rdd.map(generate_labeled_point_from_row).toDF(['label', 'features'])


def binary_black_hole_fitness_function(index_array: np.array, columns_names: np.ndarray, df: DataFrame) -> float:
    """
    Computa la funcion de fitness (F1-Score) de una estrella en particular
    :param index_array: Array binario con los features que tiene la estrella
    :param columns_names: Nombre de todas las columnas del DataFrame para poder extraer los features seleccionados
    :param df: DataFrame para extraer la informacion de los features seleccionados
    :return: F1-Score arrojado por el SVM
    """
    # En el extraño caso de que una estrella no tenga ningun feature para procesar devuelve el menor de los fitness
    if not np.count_nonzero(index_array):
        return -1.0

    # Obtiene solo los features que necesita
    parsed_data = get_parsed_data(index_array, columns_names, df)

    # Parametros a probar en el CrossValidation los dejo vacios, pero se podría probar distintos parámetros
    # del RandomForest
    param_grid = ParamGridBuilder().build()

    # Entreno SVM con Gradiente Descendente Estocastico
    crossval = CrossValidator(estimator=RandomForestClassifier(numTrees=500),
                              estimatorParamMaps=param_grid,
                              evaluator=MulticlassClassificationEvaluator(metricName='f1'),
                              numFolds=10)

    cross_model: CrossValidatorModel = crossval.fit(parsed_data)
    f1_score = cross_model.avgMetrics[0]

    # EJEMPLO VIEJO: con SVM y metrica a mano
    # model: SVMModel = SVMWithSGD.train(parsed_data, iterations=100)

    # Formateo la informacion para poder calcular las metricas
    # predictions_and_labels = parsed_data.map(lambda point: (
    #   float(best_model.predict(point.features)), point.label
    # ))

    # Instacio el calculador de metricas
    # metrics = MulticlassMetrics(predictions_and_labels)

    # Por si se quiere saber el error de entrenamiento
    # print(f'Training Error = {1 - metrics.accuracy}')

    # Devuelvo el F1-score
    # return metrics.fMeasure(label=1.0)
    
    return f1_score


def main():
    # Configuracion de Spark
    conf = SparkConf().setMaster("local").setAppName("WordCount")
    sc = SparkContext(conf=conf)
    sql = SparkSession(sc)

    # Array para guardar en formato CSV a lo ultimo
    all_experiments_results = []
    for dataset_name in ['Ovarian', 'Breast', 'Leukemia']:
        # Tomo el DataFrame del HDFS
        source_df: DataFrame = sql.read\
            .options(inferSchema='True', header='True')\
            .csv(f"/Datasets/{dataset_name}.csv")\
            .drop('_c0')

        # Arreglo los nombres de las columnas porque sino se jodia
        df_renamed = fix_columns_names(source_df)

        # Hago por cada row una clase con valor {0, 1}
        df_labeled = binarize_y(df_renamed)

        # Obtengo el numero de features
        all_columns_names = np.array(df_labeled.columns)
        n_features: int = all_columns_names.shape[0] - 1  # No cuento la columna de clases

        for run_improved in [False, True]:
            independent_start_time = time.time()
            independent_results: Dict[str, list] = get_dict_of_metrics_clean()
            final_subset = None  # Mejor subset obtenido
            best_metric = -1  # Mejor metrica obtenida
            for i in range(5):
                # Binary Black Hole
                if run_improved:
                    best_subset, best_fitness = improved_binary_black_hole(
                        n_stars=N_STARS,
                        n_features=n_features,
                        n_iterations=N_ITERATIONS,
                        fitness_function=lambda subset: binary_black_hole_fitness_function(subset, all_columns_names, df_labeled),
                        coeff_1=COEFF_1,
                        coeff_2=COEFF_2,
                        binary_threshold=None  # Usamos threshold aleatorio cada pasada (otro valor recomendado es 0.6)
                    )
                else:
                    # Binary Black Hole
                    best_subset, best_fitness = binary_black_hole(
                        n_stars=N_STARS,
                        n_features=n_features,
                        n_iterations=N_ITERATIONS,
                        fitness_function=lambda subset: binary_black_hole_fitness_function(subset, all_columns_names, df_labeled),
                        binary_threshold=None  # Usamos threshold aleatorio cada pasada (otro valor recomendado es 0.6)
                    )

                # Imprimo resultado
                selected_column_names = get_columns_by_indexes(best_subset, all_columns_names)
                print(f'Best subset -> {selected_column_names}')
                print(f'Cantidad de genes finales -> {len(selected_column_names)}')
                print(f'Best fitness -> {best_fitness}')

                # Obtiene el promedio de las 100 corridas
                subset_columns_names = get_columns_by_indexes(best_subset, all_columns_names)
                subset_x = df_labeled.select(*subset_columns_names).toPandas()
                y = df_labeled.select(CLASS_NAME).toPandas().values.ravel()
                result_100_runs_mean = get_mean_100_runs(subset_x, y)

                # Informo metricas de las 100 corridas
                # print(f'Iteracion {i + 1}, 100 corridas terminadas en {time.time() - iterations_100_start} segundos')

                # Obtengo el nombre de las columnas
                column_names = get_columns_by_indexes(best_subset, all_columns_names)

                merge_dicts(independent_results, result_100_runs_mean)

                # NOTA: solo se considera el f1 para mejor metrica
                current_accuracy = result_100_runs_mean['f1']
                if current_accuracy > best_metric:
                    best_metric = current_accuracy
                    final_subset = column_names

                # Imprime informacion del resultado
                # print(f'Resultados para {field_name}')
                # print(f'\tLa mejor combinación -> {column_names}')
                # print(f'\tBest Cross V. -> {best_fitness} (métrica "{metric}")')
                # print(f'\tReducido de {n_features} a {len(column_names)}')

            # Informo resultados finales
            independent_run_time = round(time.time() - independent_start_time, 3)
            # print(f'5 Corridas terminadas en {independent_run_time} segundos')

            experiment_results_dict = {
                'dataset': dataset_name,
                'BBHA mejorado': 1 if run_improved else 0,
                'Mejor F1 (en 5 corridas)': round(best_metric, 4),
                'Features con mejor accuracy (en 5 corridas)': ' | '.join(final_subset),
                'Tiempo de CPU (5 corridas) en segundos': independent_run_time,
            }

            for metric, value in independent_results.items():
                independent_mean = round(np.mean(value), 4)
                independent_std = round(np.std(value), 4)
                csv_result_key = f'{metric} promedio de 5 corridas'
                experiment_results_dict[csv_result_key] = f'{independent_mean} (± {independent_std})'
                # print(f'Metrica {metric} -> {independent_mean} (± {independent_std})')

            all_experiments_results.append(experiment_results_dict)
            break  # TODO: borrar
        break  # TODO: borrar
    # Guardo a CSV todos los resultados obtenidos
    now = time.strftime('%Y-%m-%d_%H:%M:%S')
    pd.DataFrame(all_experiments_results).to_json()
    pd.DataFrame(all_experiments_results).to_csv(f'./Resultados/spark_result_{now}.csv')


if __name__ == '__main__':
    main()
