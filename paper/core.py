import os
from typing import List, Optional, Callable, Union, Tuple, Any, Dict
import pandas as pd
from sklearn.feature_selection import chi2
from sklearn.preprocessing import MinMaxScaler
from pyspark.sql import DataFrame as SparkDataFrame
import numpy as np
from custom_metaheuristics import binary_black_hole, improved_binary_black_hole
from metrics import get_mean_n_runs
from paper_util import get_x_and_y, read_data, get_columns_from_df
import time
import logging

logging.getLogger().setLevel(logging.INFO)

# Algunos tipos utiles
ParameterFitnessFunctionSequential = Tuple[pd.DataFrame, np.ndarray]
ParsedDataCallable = Callable[[np.ndarray, Any, np.ndarray], Union[ParameterFitnessFunctionSequential, SparkDataFrame]]


def filter_by_chi_squared(x: pd.DataFrame, y: np.ndarray) -> List[str]:
    """
    Según el paper, se realiza un filtrado previo dejando el top de 50 features mas relevantes
    segun el test estadístico Chi Square.
    Ver -> https://towardsdatascience.com/chi-square-test-for-feature-selection-in-machine-learning-206b1f0b8223
    :param x: DataFrame con los features
    :param y: Labels
    :return: Top 50 de features mas significativos
    """
    # TODO: chequear si esta bien. chi2 no acepta numero negativos asi que estoy normalizando [0-1]
    # Escala entre [0-1]
    x_scaled = MinMaxScaler().fit_transform(x)
    scaled_df = pd.DataFrame(x_scaled, columns=x.columns, index=x.index)

    # Computa el Chi-Square
    chi_scores = chi2(scaled_df, y)

    # Ordena por p-valor y devuelve los primeros 50
    p_values = pd.Series(chi_scores[1], index=x.columns)
    p_values.sort_values(ascending=True, inplace=True)
    return p_values.index.values[:50]


def binary_black_hole_fitness_function(
    get_parsed_data: ParsedDataCallable,
    compute_cross_validation,
    index_array: np.array,
    x: pd.DataFrame,
    y: np.ndarray
) -> float:
    """
    Funcion de fitness de una estrella evaluada en el Binary Black hole
    :param get_parsed_data: Funcion para pre-procesar la informacion antes de enviarla al CrossValidation
    :param compute_cross_validation: Funcion de Cross valitadion incluida la funcion de fitness
    :param index_array: Lista de booleanos indicando cual feature debe ser incluido en la evaluacion y cual no
    :param x: Datos de los features
    :param y: Clases
    :return: Promedio de la metrica obtenida en cada fold del CrossValidation. -1 si no hay features a evaluar
    """
    if not np.count_nonzero(index_array):
        return -1.0

    (parsed_data, y) = get_parsed_data(index_array, x, y)
    return compute_cross_validation(parsed_data, y)


def run_experiment(
        datasets: List[Dict],
        get_parsed_data: ParsedDataCallable,
        compute_cross_validation: Callable[[pd.DataFrame, np.ndarray], float],
        metric_description: str,
        filter_top_50: bool = True,
        run_improved_bbha: Optional[bool] = None,
        number_of_independent_runs: int = 5,
        n_stars: int = 10,
        n_iterations: int = 25,
        coeff_1: float = 2.35,
        coeff_2: float = 0.2,
        binary_threshold: Optional[float] = None,
        number_of_runs_mean: int = 100
):
    """
    Computa el experimento con los parametros especificos
    :param datasets: Lista con los datasets a ejecutar
    :param get_parsed_data: Funcion para pre-procesar la informacion antes de enviarla al CrossValidation
    :param compute_cross_validation: Funcion de Cross valitadion incluida la funcion de fitness
    :param metric_description: Descripcion de la metrica que devuelve la funcion de CrossValidation
        para mostrar en el CSV
    :param filter_top_50: True si se quiere alimentar al BBHA con el top 50 de feature, False para usar todos
    :param run_improved_bbha: Si es None se corren ambas versiones del algoritmo.
        True para mejorado, False para original
    :param number_of_independent_runs: Numero de corridas independientes a ejecutar
    :param n_stars: Numero de estrellas a utilizar en el BBHA
    :param n_iterations: Numero de iteraciones a utilizar en el BBHA
    :param coeff_1: Coeficiente 1 requerido por la version mejorada del BBHA
    :param coeff_2: Coeficiente 2 requerido por la version mejorada del BBHA
    :param binary_threshold: Threshold usado en BBHA, None para que se compute de manera aleatoria
    :param number_of_runs_mean: Numero de corridas utilizando el subset devuelto por el BBHA
        para sacar el promedio final
    """
    experiment_start = time.time()

    # CSV donde se van a guardar las cosas
    now = time.strftime('%Y-%m-%d_%H:%M:%S')
    dir_name = os.path.dirname(__file__)
    res_csv_file_path = os.path.join(dir_name, f'Resultados/result_{now}.csv')

    logging.info(f'El archivo de resultados se guardará en {res_csv_file_path}')

    res_csv = pd.DataFrame(columns=['dataset', 'BBHA mejorado',
                                    f'Mejor {metric_description} (en {number_of_independent_runs} corridas)',
                                    f'Features con mejor accuracy (en {number_of_independent_runs} corridas)',
                                    f'Tiempo de CPU ({number_of_independent_runs} corridas) en segundos'])

    # Iteramos por los datasets que me hayan especificado
    for dataset in datasets:
        cancer_dataset, dataset_name = read_data(dataset)

        # Separa los datos para obtener X e Y
        x, y, number_classes = get_x_and_y(cancer_dataset)

        number_samples, number_features = x.shape

        logging.info(f'Dataset {dataset_name}')
        logging.info(f'\tSamples (filas) -> {number_samples} | Features (columnas) -> {number_features}')
        logging.info(f'\tNumero de clases -> {number_classes}')

        # Hace primero un Feature Selection filtrando el top por Chi Square Test
        if filter_top_50:
            top_features = filter_by_chi_squared(x, y)
            x = x[top_features]

            # Imprimo para probar
            logging.info(f'Top 50 Features obtenidos por Chi Square -> ')
            logging.info(top_features)

        # Needed parameter for the Binary Black Hole Algorithm
        n_features = x.shape[1]

        # Check which version of the algorithm want to run
        if run_improved_bbha is None:
            improved_options = [False, True]
        elif run_improved_bbha is True:
            improved_options = [True]
        else:
            improved_options = [False]

        for run_improved in improved_options:
            independent_start_time = time.time()

            final_subset = None  # Mejor subset obtenido
            best_metric = -1  # Mejor metrica obtenida
            for i in range(number_of_independent_runs):
                # Binary Black Hole
                bh_start = time.time()
                if run_improved:
                    best_subset, best_fitness = improved_binary_black_hole(
                        n_stars=n_stars,
                        n_features=n_features,
                        n_iterations=n_iterations,
                        fitness_function=lambda subset: binary_black_hole_fitness_function(
                            get_parsed_data,
                            compute_cross_validation,
                            subset,
                            x,
                            y
                        ),
                        coeff_1=coeff_1,
                        coeff_2=coeff_2,
                        binary_threshold=binary_threshold
                    )
                else:
                    best_subset, best_fitness = binary_black_hole(
                        n_stars=n_stars,
                        n_features=n_features,
                        n_iterations=n_iterations,
                        fitness_function=lambda subset: binary_black_hole_fitness_function(
                            get_parsed_data,
                            compute_cross_validation,
                            subset,
                            x,
                            y
                        ),
                        binary_threshold=binary_threshold
                    )

                logging.info(f'Corrida independiente {i + 1} | '
                             f'Binary Black Hole con {n_iterations} iteraciones y {n_stars} '
                             f'estrellas terminado en {time.time() - bh_start} segundos')

                # Computo N corridas para sacar un promedio estadistico
                (subset_parsed, y_parsed) = get_parsed_data(best_subset, x, y)
                n_iterations_start = time.time()
                current_metric = get_mean_n_runs(
                    number_of_runs_mean,
                    lambda: compute_cross_validation(subset_parsed, y_parsed)
                )
                logging.info(f'{number_of_runs_mean} corridas para el promedio terminadas en '
                             f'{time.time() - n_iterations_start} segundos')

                # Vemos si es la mejor metrica
                if current_metric > best_metric:
                    best_metric = current_metric

                    # Obtengo el nombre de las columnas
                    column_names = get_columns_from_df(best_subset, x).columns.values
                    final_subset = column_names

            # Informo resultados finales
            independent_run_time = round(time.time() - independent_start_time, 3)
            logging.info(f'{number_of_independent_runs} corridas independientes terminadas en {independent_run_time}'
                         f'segundos')

            experiment_results_dict = {
                'dataset': dataset_name,
                'BBHA mejorado': 1 if run_improved else 0,
                f'Mejor {metric_description} (en {number_of_independent_runs} corridas)': round(best_metric, 4),
                f'Features con mejor accuracy (en {number_of_independent_runs} corridas)': ' | '.join(final_subset),
                f'Tiempo de CPU ({number_of_independent_runs} corridas) en segundos': independent_run_time,
            }

            # logging.infos para debug
            algorithm = 'BBHA' + (' (improved)' if run_improved else '')
            logging.info(f'Features con {algorithm} para el dataset {dataset_name} (metrica = {best_metric}) ->')
            logging.info(final_subset)

            # Guardo a CSV con nueva info
            res_csv = res_csv.append(experiment_results_dict, ignore_index=True)
            res_csv.to_csv(res_csv_file_path)

    logging.info(f'Experimento completo terminado en {time.time() - experiment_start} segundos')
