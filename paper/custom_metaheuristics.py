from typing import Optional, Callable, Tuple
import time
import random
import numpy as np
from math import tanh


def get_random_subset_of_features(n_features: int) -> np.ndarray:
    """
    Generate a random subset of Features
    :param n_features: Number of features
    :return: Categorical array with {0, 1} values indicate the absence/presence of the feature in the index
    """
    # This while prevents returning an array with ALL zeroes
    while True:
        subset = (np.random.rand(n_features) > 0.5).astype(int)
        if np.count_nonzero(subset) > 0:
            return subset


def get_best(subsets: np.ndarray, fitness_values: np.ndarray) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """Obtiene el mayor valor de un conjunto de fitness"""
    best_idx = np.argmax(fitness_values)  # Mantengo el idx para evitar comparaciones ambiguas
    return best_idx, subsets[best_idx], fitness_values[best_idx]


def binary_black_hole(
    n_stars: int,
    n_features: int,
    n_iterations: int,
    fitness_function: Callable[[np.ndarray], float],
    binary_threshold: Optional[float] = 0.6
):
    """
    Computa la metaheuristica Binary Black Hole sacada del paper
    "Binary black hole algorithm for feature selection and classification on biological data"
    Authors: Elnaz Pashaei, Nizamettin Aydin.
    NOTA: esta hecho de manera lenta para que se pueda entender el algoritmo, usar la version vectorizada para
    produccion
    :param n_stars: Number of stars
    :param n_features: Number of features
    :param n_iterations: Number of iterations
    :param fitness_function: Fitness function to compute on every star
    :param binary_threshold: Binary threshold to set 1 or 0 the feature. If None it'll be computed randomly
    :return:
    """
    # Preparo las estructuras de datos
    stars_subsets = np.empty((n_stars, n_features), dtype=int)
    stars_fitness_values = np.empty((n_stars, ), dtype=float)

    # Inicializo las estrellas con sus subconjuntos y sus valores fitness
    for i in range(n_stars):
        random_features_to_select = get_random_subset_of_features(n_features)
        stars_subsets[i] = random_features_to_select  # Inicializa 'Population'
        stars_fitness_values[i] = fitness_function(random_features_to_select)

    # El que mejor fitness tiene es el Black Hole
    black_hole_idx, black_hole_subset, black_hole_fitness = get_best(stars_subsets, stars_fitness_values)
    # print(f'El Black Hole se situa inicialmente en la estrella {black_hole_idx}')

    # Iteraciones
    for i in range(n_iterations):
        # print(f'Iteration -> {i}')
        for a in range(n_stars):
            # Si es la misma estrella que se convirtio en agujero negro, no hago nada
            if a == black_hole_idx:
                continue

            # Compute the current star fitness
            current_star_subset = stars_subsets[a]
            current_fitness = fitness_function(current_star_subset)

            # Si la estrella tiene mejor fitness que el agujero negro, hacemos swap
            if current_fitness > black_hole_fitness:
                # print(f'Cambiando el agujero negro por la estrella {a},'
                #       f' Cur -> {current_fitness} | BH -> {black_hole_fitness}')
                black_hole_idx = a
                black_hole_subset, current_star_subset = current_star_subset, black_hole_subset
                black_hole_fitness, current_fitness = current_fitness, black_hole_fitness

            # Si la funcion de fitness es igual, pero tengo menos features en la estrella (mejor!), hacemos swap
            elif current_fitness == black_hole_fitness and np.count_nonzero(current_star_subset) < np.count_nonzero(black_hole_subset):
                # print(f'Cambiando el agujero negro por la estrella {a},'
                #       f' Cur -> {current_fitness} | BH -> {black_hole_fitness}')
                black_hole_idx = a
                black_hole_subset, current_star_subset = current_star_subset, black_hole_subset
                black_hole_fitness, current_fitness = current_fitness, black_hole_fitness

            # Calculo el horizonte de eventos
            event_horizon = black_hole_fitness / np.sum(stars_fitness_values)

            # Me fijo si la estrella cae en el horizonte de eventos
            dist_to_black_hole = np.linalg.norm(black_hole_subset - current_star_subset)  # Dist. Euclidea
            if dist_to_black_hole < event_horizon:
                # print(f'La estrella {a} cae dentro del horizonte de eventos'
                #       f'Horizonte -> {event_horizon} | Dist -> {dist_to_black_hole}')
                stars_subsets[a] = get_random_subset_of_features(n_features)

        # Actualizo de manera binaria los subsets de cada estrella
        for a in range(n_stars):
            # Salteo el agujero negro
            if black_hole_idx == a:
                continue
            for d in range(n_features):
                x_old = stars_subsets[a][d]
                threshold = binary_threshold if binary_threshold is not None else random.uniform(0, 1)
                x_new = x_old + random.uniform(0, 1) * (black_hole_subset[d] - x_old)  # Position
                stars_subsets[a][d] = 1 if abs(tanh(x_new)) > threshold else 0

    return black_hole_subset, black_hole_fitness


def improved_binary_black_hole(
    n_stars: int,
    n_features: int,
    n_iterations: int,
    fitness_function: Callable[[np.ndarray], float],
    coeff_1: float,
    coeff_2: float,
    binary_threshold: Optional[float] = 0.6
):
    """
    Computa la metaheuristica Binary Black Hole con algunas mejoras sacada del paper
    "Improved black hole and multiverse algorithms fordiscrete sizing optimization of planar structures"
    Authors: Saeed Gholizadeh, Navid Razavi & Emad Shojaei
    NOTA: esta hecho de manera lenta para que se pueda entender el algoritmo, usar la version vectorizada para
    produccion
    :param n_stars: Number of stars
    :param n_features: Number of features
    :param n_iterations: Number of iterations
    :param fitness_function: Fitness function to compute on every star
    :param coeff_1: Parametro especificado en el paper. Valores posibles = [2.2, 2.35]
    :param coeff_2: Parametro especificado en el paper. Valores posibles = [0.1, 0.2, 0.3]
    :param binary_threshold: Binary threshold to set 1 or 0 the feature. If None it'll be computed randomly
    :return:
    """
    coef_1_possible_values = [2.2, 2.35]
    coef_2_possible_values = [0.1, 0.2, 0.3]
    if coeff_1 not in coef_1_possible_values:
        print(f'El parámetro coef_1 debe ser alguno de los siguientes valores -> {coef_1_possible_values}')
        exit(1)

    if coeff_2 not in coef_2_possible_values:
        print(f'El parámetro coef_2 debe ser alguno de los siguientes valores -> {coef_2_possible_values}')
        exit(1)

    # Preparo las estructuras de datos
    stars_subsets = np.empty((n_stars, n_features), dtype=int)
    stars_best_subset = np.empty((n_stars, n_features), dtype=int)
    stars_fitness_values = np.empty((n_stars, ), dtype=float)
    stars_best_fitness_values = np.empty((n_stars, ), dtype=float)

    # Inicializo las estrellas con sus subconjuntos y sus valores fitness
    for i in range(n_stars):
        random_features_to_select = get_random_subset_of_features(n_features)
        stars_subsets[i] = random_features_to_select  # Inicializa 'Population'
        stars_fitness_values[i] = fitness_function(random_features_to_select)
        # Best fitness and position
        stars_best_fitness_values[i] = stars_fitness_values[i]
        stars_best_subset[i] = stars_subsets[i]

    # El que menor fitness tiene es el Black Hole
    black_hole_idx, black_hole_subset, black_hole_fitness = get_best(stars_subsets, stars_fitness_values)
    # print(f'El Black Hole se situa inicialmente en la estrella {black_hole_idx}')

    # Iteraciones
    for i in range(n_iterations):
        # print(f'Iteration -> {i}')
        for a in range(n_stars):
            # Si es la misma estrella que se convirtio en agujero negro, no hago nada
            if a == black_hole_idx:
                continue

            # Compute the current star fitness
            current_star_subset = stars_subsets[a]
            current_fitness = fitness_function(current_star_subset)

            # Sets best fitness and position
            if current_fitness > stars_best_fitness_values[a]:
                stars_best_fitness_values[a] = current_fitness
                stars_best_subset[a] = current_star_subset

            # Si la estrella tiene mejor fitness que el agujero negro, hacemos swap
            if current_fitness > black_hole_fitness:
                # print(f'Cambiando el agujero negro por la estrella {a},'
                #       f' Cur -> {current_fitness} | BH -> {black_hole_fitness}')
                black_hole_idx = a
                black_hole_subset, current_star_subset = current_star_subset, black_hole_subset
                black_hole_fitness, current_fitness = current_fitness, black_hole_fitness

            # Si la funcion de fitness es igual, pero tengo menos features en la estrella (mejor!), hacemos swap
            elif current_fitness == black_hole_fitness and np.count_nonzero(current_star_subset) < np.count_nonzero(black_hole_subset):
                # print(f'Cambiando el agujero negro por la estrella {a},'
                #       f' Cur -> {current_fitness} | BH -> {black_hole_fitness}')
                black_hole_idx = a
                black_hole_subset, current_star_subset = current_star_subset, black_hole_subset
                black_hole_fitness, current_fitness = current_fitness, black_hole_fitness

            # Calculo el horizonte de eventos
            # MEJORA 1: nueva funcion para definir el horizonte de eventos
            event_horizon = (1 / black_hole_fitness) / np.sum(1 / stars_fitness_values)

            # Me fijo si la estrella cae en el horizonte de eventos
            dist_to_black_hole = np.linalg.norm(black_hole_subset - current_star_subset)  # Dist. Euclidea
            if dist_to_black_hole < event_horizon:
                # print(f'La estrella {a} cae dentro del horizonte de eventos'
                #       f'Horizonte -> {event_horizon} | Dist -> {dist_to_black_hole}')

                # MEJORA 2: se cambia solo UNA dimension del arreglo de features
                random_feature_idx = random.randint(0, n_features - 1)
                stars_subsets[a][random_feature_idx] ^= 1  # Invierte el 0 o 1

        # Actualizo de manera binaria los subsets de cada estrella
        # MEJORA 3: Nueva formula de corrimiento de una estrella
        w = 1 - (i / n_iterations)
        d1 = coeff_1 + w
        d2 = coeff_2 + w

        for a in range(n_stars):
            # Salteo el agujero negro
            if black_hole_idx == a:
                continue
            for d in range(n_features):
                x_old = stars_subsets[a][d]
                x_best = stars_best_subset[a][d]
                threshold = binary_threshold if binary_threshold is not None else random.uniform(0, 1)
                bh_star_diff = black_hole_subset[d] - x_old
                star_best_fit_diff = x_best - x_old
                x_new = x_old + (d1 * random.uniform(0, 1) * bh_star_diff) + (d2 * random.uniform(0, 1) * star_best_fit_diff)
                stars_subsets[a][d] = 1 if abs(tanh(x_new)) > threshold else 0

    return black_hole_subset, black_hole_fitness


def binary_black_hole_vectorized(
    n_stars: int,
    n_features: int,
    n_iterations: int,
    fitness_function: Callable[[np.ndarray], float],
    binary_threshold: Optional[float] = 0.6
):
    """
    Computa la metaheuristica Binary Black Hole sacada del paper
    "Binary black hole algorithm for feature selection and classification on biological data"
    Authors: Elnaz Pashaei, Nizamettin Aydin.
    Es la misma implementacion que binary_black_hole() pero con la actualizacion de los features vectorizada
    :param n_stars: Number of stars
    :param n_features: Number of features
    :param n_iterations: Number of iterations
    :param fitness_function: Fitness function to compute on every star
    :param binary_threshold: Binary threshold to set 1 or 0 the feature. If None it'll be computed randomly
    :return:
    """
    bh_start = time.time()

    # Preparo las estructuras de datos
    stars_subsets = np.empty((n_stars, n_features), dtype=int)
    stars_fitness_values = np.empty((n_stars, ), dtype=float)

    # Inicializo las estrellas con sus subconjuntos y sus valores fitness
    for i in range(n_stars):
        random_features_to_select = get_random_subset_of_features(n_features)
        stars_subsets[i] = random_features_to_select  # Inicializa 'Population'
        stars_fitness_values[i] = fitness_function(random_features_to_select)

    # El que mejor fitness tiene es el Black Hole
    black_hole_idx, black_hole_subset, black_hole_fitness = get_best(stars_subsets, stars_fitness_values)
    # print(f'El Black Hole se situa inicialmente en la estrella {black_hole_idx}')

    # Iteraciones
    for i in range(n_iterations):
        # print(f'Iteration -> {i}')
        for a in range(n_stars):
            # Si es la misma estrella que se convirtio en agujero negro, no hago nada
            if a == black_hole_idx:
                continue

            # Compute the current star fitness
            current_star_subset = stars_subsets[a]
            current_fitness = fitness_function(current_star_subset)

            # Si la estrella tiene mejor fitness que el agujero negro, hacemos swap
            if current_fitness > black_hole_fitness:
                # print(f'Cambiando el agujero negro por la estrella {a},'
                #       f' Cur -> {current_fitness} | BH -> {black_hole_fitness}')
                black_hole_idx = a
                black_hole_subset, current_star_subset = current_star_subset, black_hole_subset
                black_hole_fitness, current_fitness = current_fitness, black_hole_fitness

            # Si la funcion de fitness es igual, pero tengo menos features en la estrella (mejor!), hacemos swap
            elif current_fitness == black_hole_fitness and np.count_nonzero(current_star_subset) < np.count_nonzero(black_hole_subset):
                # print(f'Cambiando el agujero negro por la estrella {a},'
                #       f' Cur -> {current_fitness} | BH -> {black_hole_fitness}')
                black_hole_idx = a
                black_hole_subset, current_star_subset = current_star_subset, black_hole_subset
                black_hole_fitness, current_fitness = current_fitness, black_hole_fitness

            # Calculo el horizonte de eventos
            event_horizon = black_hole_fitness / np.sum(stars_fitness_values)

            # Me fijo si la estrella cae en el horizonte de eventos
            dist_to_black_hole = np.linalg.norm(black_hole_subset - current_star_subset)  # Dist. Euclidea
            if dist_to_black_hole < event_horizon:
                # print(f'La estrella {a} cae dentro del horizonte de eventos'
                #       f'Horizonte -> {event_horizon} | Dist -> {dist_to_black_hole}')
                stars_subsets[a] = get_random_subset_of_features(n_features)

        # Actualizo de manera binaria los subsets de cada estrella
        for a in range(n_stars):
            # Salteo el agujero negro
            if black_hole_idx == a:
                continue
            x_old = stars_subsets[a]
            threshold = binary_threshold if binary_threshold is not None else np.random.rand(n_features)
            x_new = x_old + np.random.rand(n_features) * (black_hole_subset - x_old)  # Position
            condition = abs(np.tanh(x_new)) > threshold
            stars_subsets[a][condition] = 1
            stars_subsets[a][~condition] = 0

    print(f'Binary Black Hole con {n_iterations} iteraciones y {n_stars} '
          f'estrellas terminado en {time.time() - bh_start} segundos')
    return black_hole_subset, black_hole_fitness
