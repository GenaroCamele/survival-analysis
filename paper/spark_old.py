# IMPORTANTE: contiene las pruebas para implementar un paralelismo a nivel de estrellas.
# Este enfoque no se puede implementar a menos que se cuente con una réplica completa de los datos en cada
# uno de los workers, lo cual no es propicio para un entorno Big Data

from operator import add
from pyspark import RDD
from pyspark import SparkConf, SparkContext
from typing import Optional
from pyspark.sql import SparkSession
import numpy as np
from pyspark.sql import DataFrame
from pyspark.ml.feature import StringIndexer

# Instancio lo necesario para Spark
conf = SparkConf().setMaster("local").setAppName("WordCount")
sc = SparkContext(conf=conf)
sql = SparkSession(sc)

CLASS_NAME = 'class'


def fix_columns_names(df):
	temp_list = []  # Edit01
	for col in df.columns:
		new_name = col.strip()
		new_name = "".join(new_name.split())
		new_name = new_name.replace('.', '_')  # EDIT
		temp_list.append(new_name)  # Edit02
	return df.toDF(*temp_list)


def binarize_y(df: DataFrame) -> DataFrame:
	new_class_name = f'{CLASS_NAME}_new'
	string_indexer = StringIndexer(inputCol=CLASS_NAME, outputCol=new_class_name)
	model = string_indexer.fit(df)
	return model.transform(df).drop(CLASS_NAME).withColumnRenamed(new_class_name, CLASS_NAME)


def initialize_star(row, fitness_function, n_features):
	random_features_to_select = get_random_subset_of_features(n_features)
	return (
		row[0],
		random_features_to_select,
		fitness_function(random_features_to_select)
	)


def get_random_subset_of_features(n_features: int) -> np.ndarray:
	# This while prevents returning an array with ALL zeroes
	while True:
		subset = (np.random.rand(n_features) > 0.5).astype(int)
		if np.count_nonzero(subset) > 0:
			return subset


def row_expander(row):
	row_dict = row.asDict()
	# row_dict.pop('_c0')
	for k in row_dict:
		yield k, (row[k],)


def get_best(stars):
	best = stars.max(lambda star: star[2])  # Mantengo el idx para evitar comparaciones ambiguas
	return best[0], sc.parallelize([best]), np.count_nonzero(best[1])


def compare_star_bh(row, fitness_function):
	star = row[0]
	bh = row[1]
	current_star_subset = star[1]
	current_fitness = fitness_function(current_star_subset)
	return star[0], star[1], current_fitness, bh[2]


def filter_by_best(row, bh_number_of_features: int):
	return row[2] > row[3] or (row[2] == row[3] and np.count_nonzero(row[1]) < bh_number_of_features)


def update_stars_inside_event_horizon(star_and_bh, event_horizon, n_features, binary_threshold):
	star = star_and_bh[0]
	black_hole = star_and_bh[1]
	if star[0] == black_hole[0]:
		return star

	dist = np.linalg.norm(black_hole[1] - star[1])
	if dist > event_horizon:
		new_star = star
	else:
		# Genera nuevas "coordenadas" para la estrella
		new_star = (star[0], get_random_subset_of_features(n_features), None)

	# Computamos el "movimiento" de la estrella hacia el Black Hole
	return update_stars_position(new_star, black_hole, binary_threshold)


def update_stars_position(star, black_hole, binary_threshold):
	x_old = star[1]
	threshold = binary_threshold if binary_threshold is not None else random.uniform(0, 1)
	x_new = x_old + random.uniform(0, 1) * (black_hole[1] - x_old)  # Position
	cond = np.absolute(np.tanh(x_new)) > threshold
	x_new[cond] = 1
	x_new[~cond] = 0
	return star[0], x_new, None


def binary_black_hole(n_stars, n_iterations, n_features, fitness_function, binary_threshold):
	# Preparo las estructuras de datos
	stars = sc.parallelize(((x,) for x in range(n_stars))).map(
		lambda row: initialize_star(row, fitness_function, n_features))

	# El que mejor fitness tiene es el Black Hole
	black_hole_key, black_hole, bh_number_of_features = get_best(stars)

	for _ in range(n_iterations):
		stars_except_bh = stars.filter(lambda row: row[0] != black_hole_key)  # No computa el Black Hole
		stars_and_bh = stars_except_bh.cartesian(black_hole)  # Cartesiano para poder comparar con el Black Hole

		# Genera una comparacion con el Black Hole para cada una de las estrellas
		compared = stars_and_bh.map(lambda row: compare_star_bh(row, fitness_function))

		# Se queda con las que tienen mejor fitness o igual fitness pero menor cantidad de features
		bh_candidates = compared.filter(lambda row: filter_by_best(row, bh_number_of_features)).map(
			lambda row: (row[0], row[1], row[2]))
		bh_candidates.persist()  # Necesario para que el isEmpty() funcione

		# Computo el nuevo Black Hole
		# Solo si hay candidatos
		if not bh_candidates.isEmpty():
			black_hole_key, black_hole, bh_number_of_features = get_best(bh_candidates)
			print(f'bh_number_of_features -> {bh_number_of_features}')

		# Computes event horizon
		event_horizon = black_hole.first()[2] / compared.map(lambda row: row[2]).sum()

		# Si caen en el horizonte de eventos genero una nueva posicion aleatoria.
		# Despues se actualiza la "posicion" de cada estrella
		stars = stars.cartesian(black_hole).map(lambda star_and_bh: update_stars_inside_event_horizon(
			star_and_bh,
			event_horizon,
			n_features,
			binary_threshold
		))

		stars.persist()

	return black_hole.first()

# +++++++++++++++++++++++++++++ PARA LEVANTAR Y PARSEAR DESDE EL CSV +++++++++++++++++++++++++++++

df = sql.read.options(inferSchema='True', header='True').csv("/Datasets/Ovarian.csv").drop('_c0')

n_features = len(df.columns) - 1,  # No cuento la columna de clases

# Hago por cada row una clase con valor {0, 1}
new_df = binarize_y(df)

# Arreglos los nombres de las columnas porque sino se jodia
new_df = fix_columns_names(new_df)

# Hago de cada columna una tupla de [<nombre columna>, <valor de la Row para esa columna>
# column_value = new_df.rdd.flatMap(row_expander)

# Transpuesta para hacer el join: se agrupa por clave para que me quede [<nombre columna>, <valores de todas las rows>
# Se usa reduceByKey para no generar trafico innecesario.
# Leer: https://www.edureka.co/community/11996/groupbykey-vs-reducebykey-in-apache-spark
# grouped = column_value.reduceByKey(add)

# grouped.saveAsTextFile('/Datasets/Ovarian-parseado2.txt')

# +++++++++++++++++++++++++++++ PARA LEVANTAR Y PARSEAR DESDE EL CSV +++++++++++++++++++++++++++++

# +++++++++++++++++++++++++++++ PARA LEVANTAR YA PARSEADO DESDE TXT +++++++++++++++++++++++++++++


def parse_line(line):
	line = line.replace('"', '').replace('(', '').replace(')', '').split(',')
	key = line[0].replace('\'', '')
	if key != 'class':
		return key, [float(elem) for elem in line[1:]]
	return key, line[1:]


grouped_2 = sc.textFile('/Datasets/Ovarian-parseado2.txt').map(parse_line)
x = grouped_2.filter(lambda row: row[0] != 'class')
y = grouped_2.filter(lambda row: row[0] == 'class')
n_features = len(x.first()[1])

# +++++++++++++++++++++++++++++ PARA LEVANTAR YA PARSEADO DESDE TXT +++++++++++++++++++++++++++++

import random
fitness_function = lambda _: random.uniform(0, 1)  # TODO: corregir por el SVM


# n_stars = 10
# n_iterations = 25
n_stars = 5
n_iterations = 5
binary_threshold: Optional[float] = None

_, best_subset, best_fitness = binary_black_hole(n_stars, n_iterations, n_features, fitness_function, binary_threshold)
# print(black_hole_obj)
# best_subset, best_fitness = black_hole_obj[1], black_hole_obj[2]
non_zero_idx = np.nonzero(best_subset)
df = sql.read.options(inferSchema='True', header='True').csv("/Datasets/Ovarian.csv").drop('_c0')
columns = np.array(df.columns)
column_names = columns[non_zero_idx]

print(f'Best subset -> {column_names}')
print(f'Best fitness -> {best_fitness}')
