import os
from typing import Tuple, Dict
import numpy as np
import pandas as pd
from scipy.io import arff
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import label_binarize

# Nombre de la columna de la clase en los DataFrame
NEW_CLASS_NAME = 'class'


def specificity(y_true, y_pred):
    """
    Computa la metrica de Especificidad ya que Sklearn no lo implementa en la libreria
    Solucion sacada de https://stackoverflow.com/questions/33275461/specificity-in-scikit-learn
    :param y_true: Y true
    :param y_pred: Y pred
    :return: Especificidad
    """
    conf_res = confusion_matrix(y_true, y_pred).ravel()
    tn, fp = conf_res[0], conf_res[1]
    return tn / (tn + fp)


def get_arff_as_df(path: str) -> pd.DataFrame:
    """
    Transforms an ARFF file to a Pandas' DataFrame
    :param path: Path of the ARFF file
    :return: DataFrame
    """
    return pd.DataFrame(arff.loadarff(path)[0])


def rename_class_column_name(df: pd.DataFrame, class_name_old: str):
    """
    Renames the DataFrame class column to generalize the algorithms
    :param df: DataFrame
    :param class_name_old: Current class column name
    """
    df.rename(columns={class_name_old: NEW_CLASS_NAME}, inplace=True)


def get_x_and_y(df: pd.DataFrame) -> Tuple[pd.DataFrame, np.ndarray, int]:
    """
    Gets X and Y from a DataFrame
    :param df: DataFrame to split
    :return: X and Y
    """
    # Hay que binarizar porque sino el recall arroja error
    y, number_classes = binarize_y(df[NEW_CLASS_NAME])
    return df.loc[:, df.columns != NEW_CLASS_NAME], y, number_classes


def read_data(dataset: Dict[str, str]) -> Tuple[pd.DataFrame, str]:
    """
    Lee los datos de todos los datasets definidos
    :return: Iterador con la informacion y el nombre del dataset leido en el momento
    """
    field_name = dataset["file_name"]
    file_path = os.path.join(os.path.dirname(__file__), f'Datasets/{field_name}.arff')
    data = get_arff_as_df(file_path)
    rename_class_column_name(data, dataset['class_col_name'])
    return data, field_name


def binarize_y(y: pd.Series) -> Tuple[np.ndarray, int]:
    """
    Genera un arreglo de binarios indicando la clase
    :param y: Arreglo con la clase
    :return: Arreglo categorico binario
    """
    y = y.str.decode("utf-8")
    classes = y.unique()
    return label_binarize(y, classes=classes).ravel(), classes.shape[0]


def get_columns_by_categorical(columns_index: np.ndarray, df: pd.DataFrame) -> pd.DataFrame:
    """
    Obtiene las columnas a partir de un arrego categorico
    :param columns_index: Numpy Array with a {0, 1} in the column index to indicate absence/presence of the column
    :param df: DataFrame to retrieve the columns data
    :return: DataFrame with only the specified columns
    """
    non_zero_idx = np.nonzero(columns_index)
    return df.iloc[:, non_zero_idx[0]]


def get_columns_from_df(columns_list: np.array, df: pd.DataFrame) -> pd.DataFrame:
    """Devuelve un conjunto de columnas de un DataFrame. La utilidad de este metodo es que funciona
    para indices categoricos o strings"""
    if np.issubdtype(columns_list.dtype, np.number):
        # Obtengo por indices enteros
        return get_columns_by_categorical(columns_list, df)
    # Obtengo por string/label de columna
    return df[columns_list]
