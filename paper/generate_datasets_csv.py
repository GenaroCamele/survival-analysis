# Este script se encarga de generar los .csv para cada dataset en formato .arff
from paper.paper_util import read_data


def main():
    for cancer_dataset, dataset_name in read_data():
        cancer_dataset.to_csv(f'./Datasets/{dataset_name}.csv')


if __name__ == '__main__':
    main()

