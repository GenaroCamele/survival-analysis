# En este archivo defino los distintos tipos de optimizaciones que se pueden utilizar para optimizar el algoritmo
from abc import abstractmethod
from typing import Any, Tuple, TypeVar
import numpy as np
from util import compute_survival_curve
from typing_extensions import Literal
from sksurv.metrics import concordance_index_censored
from sklearn.metrics import roc_auc_score
# TODO: agregar doc de cada clase

OptimizationTypes = Literal['logrank_p-value', 'logrank_test_statistic', 'c-index', 'anlp', 'roc']


class Optimization:
    """Super clase"""
    initial_score_value: float
    score_name: str

    @abstractmethod
    def compute_score(
            self,
            best_scoring_value: Any,
            duration_1: np.ndarray,
            event_observed_1: np.ndarray,
            duration_2: np.ndarray,
            event_observed_2: np.ndarray,
    ) -> Tuple[Any, bool]: raise NotImplementedError


class LogrankTestPValue:
    def __init__(self):
        self.initial_score_value = 9999.0
        self.score_name = 'p-value'

    @staticmethod
    def compute_score(
        best_scoring_value: Any,
        duration_1: np.ndarray,
        event_observed_1: np.ndarray,
        duration_2: np.ndarray,
        event_observed_2: np.ndarray,
    ) -> Tuple[Any, bool]:
        log_rank = compute_survival_curve(
            duration_1,
            event_observed_1,
            duration_2,
            event_observed_2,
            plot=False
        )

        # Chequeo si el p-value es menor
        current_score = log_rank.p_value
        return current_score, best_scoring_value > current_score


class LogrankTestTestStatistic:
    def __init__(self):
        self.initial_score_value = -1
        self.score_name = 'test statistic'

    @staticmethod
    def compute_score(
        best_scoring_value: Any,
        duration_1: np.ndarray,
        event_observed_1: np.ndarray,
        duration_2: np.ndarray,
        event_observed_2: np.ndarray,
    ) -> Tuple[Any, bool]:
        log_rank = compute_survival_curve(
            duration_1,
            event_observed_1,
            duration_2,
            event_observed_2,
            plot=False
        )

        # Chequeo si el test_statistic es mayor
        current_score = log_rank.test_statistic
        return current_score, best_scoring_value < current_score


class ConcordanceIndex:
    def __init__(self):
        self.initial_score_value = -1.0
        self.score_name = 'c-index'

    @staticmethod
    def compute_score(
        best_scoring_value: Any,
        duration_1: np.ndarray,
        event_observed_1: np.ndarray,
        duration_2: np.ndarray,
        event_observed_2: np.ndarray,
    ) -> Tuple[Any, bool]:
        event_indicator = np.concatenate((event_observed_1, event_observed_2))
        time_indicator = np.concatenate((duration_1, duration_2))
        print(event_indicator)
        print(time_indicator)
        exit()
        # FIXME: me falta el parametro estimate que no tengo idea de como se saca
        metric = concordance_index_censored(event_indicator, time_indicator)

        # Chequeo si el test_statistic es mayor
        current_score = metric.cindex
        return current_score, best_scoring_value < current_score


class RocCurve:
    def __init__(self):
        self.initial_score_value = -1.0
        self.score_name = 'AUC'

    @staticmethod
    def compute_score(
        best_scoring_value: Any,
        duration_1: np.ndarray,
        event_observed_1: np.ndarray,
        duration_2: np.ndarray,
        event_observed_2: np.ndarray,
    ) -> Tuple[Any, bool]:
        y_true = np.concatenate((event_observed_1, event_observed_2))
        y_pred = np.concatenate((np.zeros(event_observed_1.shape[0]), np.ones(event_observed_2.shape[0])))
        # print(y_true)
        # print(y_true.shape)
        # print(y_pred)
        # print(y_pred.shape)
        # exit()

        current_score = roc_auc_score(y_true, y_pred)

        # Chequeo si el area baja la curva es mayor
        return current_score, best_scoring_value < current_score


def get_optimization(opt_type: OptimizationTypes):
    if opt_type == 'logrank_p-value':
        return LogrankTestPValue()
    if opt_type == 'logrank_test_statistic':
        return LogrankTestTestStatistic()
