# Survival Analysis

**Antes que nada, si se quiere ver lo referente al paper del algoritmo Binary Black Hole se debe ~~leer el [README.md](paper/README.md) que se encuentra dentro de la carpeta `paper`.~~ acceder al repositorio de [Paper Binary Black Hole Algorithm](https://bitbucket.org/GenaroCamele/paper-binary-black-hole-algorithm/src).**



## Cómo correr

1. Crear el entorno de Python:
	1. `python3 -m venv venv` (una única vez)
	1. `source venv/bin/activate` (esto se debe hacer al inicio de cada sesión de consola)
1. Instalar las dependencias (una única vez): `pip install -r requirements.txt`
1. Correr el script que se quiera: `python3 <script.py>`


## Archivos

- Aquellos archivos que se encuentran en la carpeta *tutorials* contienen algoritmos sacados de blogs y tutoriales de internet como introducción a las curvas Kaplan-Meier y algunas métricas como el LogRank Test.
- El script *util.py* contiene los algoritmos de búsqueda, agrupamiento, etc para poder reutilizarlos con los datasets que se quiera.
- El script *group_dataset_chico.py* aplica los algoritmos sobre un dataset chico sacado que manejamos en Multiomics para pruebas rápidas.
- El script *group_tcga_breast.py* aplica los algoritmos sobre un dataset muy grande sacado de [cBioPortal](https://www.cbioportal.org/). Este requiere al archivo *survival_tcga_breast/data_mRNA_median_Zscores.txt* que se debe descargar de [acá](http://download.cbioportal.org/brca_tcga_pub.tar.gz).
- El resto son para funcionamiento interno o de los tutoriales.

## Cosas interesantes para leer o probar

1. [X] Leer sobre análisis de supervivencia.
	1. Importante el concepto de *censored data*.
	1. La lectura se puede consultar en mi Zotero.
1. [X] Leer sobre *Logrank Test*, *Concordance Index*, *Curvas ROC* y Cox (proportional hazards) regression que nos permiten establecer las variables objetivo a optimizar.
1. [X] Leer el paper de introducción al feature selection.
    1. Hay un buen curso sobre Feature Selection aplicado a varios datasets utilizando Python [acá](https://heartbeat.fritz.ai/hands-on-with-feature-selection-techniques-filter-methods-f248e0436ce5)
1. [X] Leer los dos papers que pasó Facu sobre Deep Learning en Survival Analysis. (Ver Zotero)
	1. [X] Ver si solucionan el problema de tener en conjunto el tiempo y el evento como variables objetivo. **Si, ver las notas en Zotero**
1. [X] Implementar la evaluación (Holdout) de un % del dataset. Mostrar las imágenes de training y evaluación con títulos para diferenciarlas y compararlas.
1. Aplicar el Blind Search con LogRank (Estadístico, no pvalue) al dataset que mandó Mati a ver que tira.
1. Ver los modelos de Deep Learning.
1. Implementar un Feature Selection del tipo Filter por correlacion.
1. Implementar un Blind Search comparando las métricas:
	1. [X] [Logrank test](https://lifelines.readthedocs.io/en/latest/lifelines.statistics.html#lifelines.statistics.logrank_test). 
		1. [X] Por p-value menor.
		1. [X] Por test estadístico mayor (campo *test_statistic*). 
	1. [Concordance Index](https://scikit-survival.readthedocs.io/en/latest/generated/sksurv.metrics.concordance_index_censored.html). 
	1. [Average Negative Log Probability](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.log_loss.html).
	1. [Curvas ROC](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.roc_curve.html).
1. Averiguar paralelización del Blind Search.
1. Investigar metaheurísticas.

## Para más adelante
1. Descargar un Dataset de CBioPortal
	1. Ver lo de Breast Cancer (TCGA 2012). El biomarcador oncotype dx es famoso, sirve de ejemplo de objetivo
1. Establecer modelo de ML para predicción
	1. Establecer métricas a partir del análisis de supervivencia Logrank Test, Concordance Index y Curvas ROC. Averiguar si hay más.
	1. El modelo: 
1. Hacer feature selection y ver si podemos obtener un subconjunto con mejores métricas.
1. Analizar metaheurísticas con algunos datasets acotados donde analizar todo el espacio de soluciones sea resoluble para poder comparar tiempos y respuestas obtenidas.


Análisis de supervivencia:
	- Tiempo que pasó hasta el evento (recidiva o muerte)
	- Ocurrió (recidivó o murió) o no ocurrió (la cantidad de meses indica los meses pasados hasta la última vez que se vió al paciento desde la cirujía)


¿Cómo se está haciendo ahora (Bioplat)?
	- Se toma una foto de las expresiones génicas al momento de hacer la cirujía (o previo a ella, o al momento de hacer el diagnóstico) con el objetivo de saber en cúal de las 4 clases de riesgo de recidiva cae (es lo que hace el oncotype dx).
	- Clusterizar a los pacientes por expresion
	- Medir la correlación de los samples de los clusters resultantes con los datos de supervivencia
		- Esta correlación se calculaba utilizando Logrank Test, Concordance Index y Curvas ROC
	- El PSO se optimizaba buscando un agrupamiento que genere clusters que aumenten esa correlación


Averiguar con Facu a ver si se puede cambiar las capas de entrada. Plantear el PSO para ver si se puede generar una red neuronal general.
Preguntar si existen modelos de ML que permitan hacer feature selection.