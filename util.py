import itertools
import time
from typing import Tuple, Optional, List, Any
from typing_extensions import Literal
import numpy as np
import pandas as pd
from lifelines import KaplanMeierFitter
from lifelines.statistics import logrank_test, StatisticalResult
from sklearn.cluster import KMeans
from scipy.stats import shapiro
from matplotlib import pyplot as plt

# Para evitar los warnings de Pandas al setear una columna sobre un slice
pd.options.mode.chained_assignment = None

CorrType = Literal['pearson', 'kendall', 'spearman']


def compute_survival_curve(
        duration_1: np.ndarray,
        event_observed_1: np.ndarray,
        duration_2: np.ndarray,
        event_observed_2: np.ndarray,
        plot: bool = True
) -> StatisticalResult:
    """
    Grafica las curvas de supervivencia generadas con Kaplan-Meier. Devuelve el valor logrank
    :param duration_1: Array Numpy con los valores de tiempo del primer grupo
    :param event_observed_1: Array Numpy con los valores de evento (0, 1) del primer grupo
    :param duration_2: Array Numpy con los valores de tiempo del segundo grupo
    :param event_observed_2: Array Numpy con los valores de evento (0, 1) del segundo grupo
    :param plot: Si esta en True plotea la curva Kaplan-Meier, sino solo se lmita a calcula el LogRank Test
    :return: Resultado del LogRank Test
    """
    if plot:
        kmf = KaplanMeierFitter()
        ax = plt.subplot(111)
        ax = kmf.fit(duration_1, event_observed_1, label="Group 1").plot(ax=ax)
        kmf.fit(duration_2, event_observed_2, label="Group 2").plot(ax=ax)
        plt.show()
    return logrank_test(duration_1, duration_2, event_observed_A=event_observed_1, event_observed_B=event_observed_2)


def blind_search(
        gen_df: pd.DataFrame,
        clinical_df: pd.DataFrame,
        time_field: str,
        event_field: str,
        event_living_value: Any,
        event_deceased_value: Any,
        optimization,
        split: bool = False,
        max_n_combinations: Optional[int] = None
) -> Tuple[float, List[str], Tuple[Any, Any, Any, Any], KMeans]:
    """
    Hace una busqueda en el espacio de soluciones completo optimizando Logrank test. Devuelve
    el pvalue menor y la mejor combinacion de genes
    :param gen_df: DataFrame con los datos de expresiones genicas
    :param clinical_df: DataFrame con los datos de supervivencia
    :param time_field: Nombre del campo que contiene la informacion del tiempo que transcurrio
    :param event_field: Nombre del campo que contiene la informacion del evento (0, 1) que transcurrio
    :param event_living_value: Valor que indica que un paciente esta vivo en el DataFrame de datos de supervivencia
    :param event_deceased_value: Valor que indica que un paciente esta muerto en el DataFrame de datos de supervivencia
    :param optimization: Algoritmo de optimization para utilizar para separar los grupos
    :param split: Si esta en True se divide el dataset en train, val y test, imprimiendo info sobre el mejor modelo
    :param max_n_combinations: Si se especifica, se limita a probar todas las combinaciones ente 1..max_n_combinations
    Sino se prueban todas
    :return: Devuelve el mejor p_value, la mejor combinacion de genes, los datos para graficar la curva Kaplan-Meier
    obtenidos durante la busqueda y el modelo Kmeans optimo
    """
    # Preparamos algunas variables utiles
    genes_list = gen_df.columns.values
    best_model: Optional[KMeans] = None
    best_scoring_value: float = optimization.initial_score_value
    score_name: str = optimization.score_name
    best_combination: List[str] = []
    best_kaplan_meier: Tuple[Any, Any, Any, Any] = (None, None, None, None)

    # Calculo la cantidad de elementos maximos a combinar
    genes_count = len(genes_list)
    n = max_n_combinations + 1 if max_n_combinations is not None else genes_count + 1

    # Hace un join con clinical data
    merged = gen_df.join(clinical_df, how='inner', lsuffix='_caller', rsuffix='_other')

    if split:
        train, val, test = get_trainning_test_validation_split(merged)
    else:
        train, val, test = merged, None, None

    all_time = time.time()
    # Arranca desde 1 porque no considero el caso de tupla vacia
    for L in range(1, n):
        l_time = time.time()
        print(f'Probando combinaciones con {L} genes de {genes_count} genes totales')

        # NOTA: la cantidad de combinaciones posibles que se haran es n! / L! / (n - L)!
        for subset in itertools.combinations(genes_list, L):
            # Casteo a lista o va a arrojar un error
            subset = list(subset)
            subset_gen_df = train[subset]

            # Agrupo
            model = group_using_k_means(subset_gen_df)
            train['group'] = model.labels_

            if train.isnull().any().any():
                print("Hay valores nulos!", train.isnull().any())
                exit()

            # Separa en ambos grupos y grafica las curvas Kaplan-Meier
            group_1, group_2 = get_groups(
                train,
                event_field=event_field,
                event_living_value=event_living_value,
                event_deceased_value=event_deceased_value
            )

            duration_1 = group_1[time_field]
            event_observed_1 = pd.to_numeric(group_1[event_field])  # Paso a numerico para evitar un warning
            duration_2 = group_2[time_field]
            event_observed_2 = pd.to_numeric(group_2[event_field])  # Paso a numerico para evitar un warning

            # Check if the metrics are better
            current_score, is_better = optimization.compute_score(
                best_scoring_value,
                duration_1,
                event_observed_1,
                duration_2,
                event_observed_2,
            )

            # Evaluo el logrank test para ver si es la mejor combinacion
            if is_better:
                best_model = model
                best_scoring_value = current_score
                best_combination = subset
                best_kaplan_meier = (
                    duration_1,
                    event_observed_1,
                    duration_2,
                    event_observed_2
                )

        print(f'Busqueda con todas las combinaciones de {L} genes terminada en {time.time() - l_time} segundos')

        print(f'Mejor combinacion hasta ahora -> {best_combination} ({score_name} -> {best_scoring_value})')

        # Imprimo informacion acerca de los datasets de testing y validacion
        if split:
            p_value_test, test_statistic_test = get_logrank_info(
                best_model, val, best_combination, event_field, time_field, event_living_value, event_deceased_value
            )
            p_value_val, test_statistic_val = get_logrank_info(
                best_model, test, best_combination, event_field, time_field, event_living_value, event_deceased_value
            )
            print('Dicha combinación arrojó')
            print(f'\tUn pvalue -> {p_value_test} y un estadístico {test_statistic_test} en test')
            print(f'\tUn pvalue -> {p_value_val} y un estadistico {test_statistic_val} en validación')

    print(f'Blind search terminada en {time.time() - all_time} segundos')

    # Devuelvo los mejores parametros
    return best_scoring_value, best_combination, best_kaplan_meier, best_model


def group_using_k_means(df: pd.DataFrame) -> KMeans:
    """
    Agrupa por expresiones de genes utilizando K-Means
    :param df: DataFrame con los datos que se agruparan
    :return: Modelo entrenado
    """
    return KMeans(n_clusters=2).fit(df.values)


def get_groups(
        merged: pd.DataFrame,
        event_field: str,
        event_living_value: Any,
        event_deceased_value: Any
) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Formatea el estado de los pacientes a lo largo del tiempo y divide el DF a partir del cluster al que
    pertecen segun K-Means
    :param merged: DataFrame con la informacion de expresiones y de supervivencia puestas juntas
    :param event_field: Nombre del campo que contiene la informacion del evento (0, 1) que transcurrio
    :param event_living_value: Valor que indica que un paciente esta vivo en el DataFrame de datos de supervivencia
    :param event_deceased_value: Valor que indica que un paciente esta muerto en el DataFrame de datos de supervivencia
    :return: Dos DataFrames, uno por cada grupo de pacientes
    """
    merged.loc[(merged[event_field] == event_deceased_value), event_field] = 0
    merged.loc[(merged[event_field] == event_living_value), event_field] = 1
    group_1 = merged[merged['group'] == 0]
    group_2 = merged[merged['group'] == 1]
    return group_1, group_2


def analyse_genes_list(
        df: pd.DataFrame,
        genes_list: List[str],
        time_field: str,
        event_field: str,
        event_living_value: Any,
        event_deceased_value: Any,
):
    """
    Imprime informacion util sobre un subconjunto de genes. Util para debuguear cosas y generar los graficos de
    supuestos conjuntos de genes optimos
    :param df: DataFrame con los datos de expresiones genicas y clinicos
    :param genes_list: Lista de genes a probar
    :param time_field: Nombre del campo que contiene la informacion del tiempo que transcurrio
    :param event_field: Nombre del campo que contiene la informacion del evento (0, 1) que transcurrio
    :param event_living_value: Valor que indica que un paciente esta vivo en el DataFrame de datos de supervivencia
    :param event_deceased_value: Valor que indica que un paciente esta muerto en el DataFrame de datos de supervivencia
    :return:
    """
    # Agrupo
    df['group'] = group_using_k_means(df[genes_list]).labels_

    # Separa en ambos grupos y grafica las curvas Kaplan-Meier
    group_1, group_2 = get_groups(
        df,
        event_field=event_field,
        event_living_value=event_living_value,
        event_deceased_value=event_deceased_value
    )

    log_rank = compute_survival_curve(
        group_1[time_field],
        pd.to_numeric(group_1[event_field]),  # Paso a numerico para evitar un warning
        group_2[time_field],
        pd.to_numeric(group_2[event_field]),  # Paso a numerico para evitar un warning
        plot=True
    )

    print(f'Resultado de LogRank con los genes {genes_list}. P-Value -> {log_rank.p_value}')
    log_rank.print_summary()


def get_trainning_test_validation_split(df: pd.DataFrame) -> Tuple[Any, Any, Any]:
    """
    Genera splits de trainning, test y validacion en una proporcion de 80%, 20%, 20%
    :param df: DataFrame a dividir
    :return: 3 splits, train, validate, test (80%, 20%, 20%)
    """
    df_len = len(df)
    return np.split(df.sample(frac=1), [int(.6 * df_len), int(.8 * df_len)])


def get_logrank_info(model, df, genes_list, event_field, time_field, event_living_value, event_deceased_value) \
        -> Tuple[float, float]:
    groups_values = model.predict(df[genes_list])
    df['group'] = groups_values

    group_1, group_2 = get_groups(
        df,
        event_field=event_field,
        event_living_value=event_living_value,
        event_deceased_value=event_deceased_value
    )

    log_rank_val = compute_survival_curve(
        group_1[time_field],
        pd.to_numeric(group_1[event_field]),  # Paso a numerico para evitar un warning
        group_2[time_field],
        pd.to_numeric(group_2[event_field]),  # Paso a numerico para evitar un warning,
        plot=False
    )

    return log_rank_val.p_value, log_rank_val.test_statistic


def is_normally_distributed(df: pd.DataFrame, alpha: float = 0.05, show: bool = False) -> bool:
    """
    Verifica que los datos esten normalmente distribuidos (hipotesis nula H0) usando el metodo de Shapiro
    :param df: Datos a chequear
    :param alpha: Alpha a testear, si el pvalue es mayor a este alpha entonces se puede rechazar H0
    :param show: Si es True si plotea el histograma
    :return: True si estan normalmente distribuidos, False caso contrario
    """
    if show:
        plt.hist(df, bins=50)
        plt.show()
    _, p_value = shapiro(df)
    return p_value > alpha


def get_most_related_pairs(df: pd.DataFrame, method: CorrType = 'pearson', n: int = 5) -> pd.Series:
    """
    Computa los pares de genes mas correlacionados
    :param df: DataFrame para computa la correlacion entre todos los pares
    :param method: Metodo de correlacion a utilizar: 'pearson', 'kendall', 'spearman'
    :param n: Cantidad de pares a devolver (estan ordenados decrecientemente)
    :return: Series de Pandas con los N pares de genes mas correlacionados
    """
    corr_matrix = df.corr(method=method).abs()

    # the matrix is symmetric so we need to extract upper triangle matrix without diagonal (k = 1)
    sol = (corr_matrix.where(np.triu(np.ones(corr_matrix.shape), k=1).astype(np.bool))
           .stack()
           .sort_values(ascending=False))
    return sol[0: n + 1]


def filter_by_corr(
    df: pd.DataFrame,
    pairs_to_remove: pd.Series,
    threshold: float,
    inplace: bool = False
) -> pd.DataFrame:
    """
    Filtra un DataFrame por los pares indicados que tienen una correlacion por encima del threshold establecido
    :param df: DataFrame a filtrar
    :param pairs_to_remove: Series con los pares de claves y su correlacion (resultado de get_most_related_pairs)
    :param threshold: Threshold para saber si la correlacion es lo suficientemente alta como para eliminar la clave
    :param inplace: Si esta en True modifica df, sino devuelve una copia dejando df inalterado
    :return: DataFrame filtrado
    """
    # Obtengo la que son mas altas que el threshold
    keys_with_high_cor = pairs_to_remove.loc[lambda x:  x > threshold]
    keys_to_remove = keys_with_high_cor.index.values

    # Remuevo las claves duplicadas. El indice [0] se debe a que es un par de claves, solo tomamos la primera
    keys_not_duplicated = list(set([pair[0] for pair in keys_to_remove]))

    # Remuevo las claves del DataFrame
    res = df if inplace else df.copy()
    for key in keys_not_duplicated:
        res.drop(key, axis=1, inplace=True)
    return res


def get_columns_by_categorical(columns_index: np.ndarray, df: pd.DataFrame) -> pd.DataFrame:
    """
    Obtiene las columnas a partir de un arrego categorico
    :param columns_index: Numpy Array with a {0, 1} in the column index to indicate absence/presence of the column
    :param df: DataFrame to retrieve the columns data
    :return: DataFrame with only the specified columns
    """
    non_zero_idx = np.nonzero(columns_index)
    return df.iloc[:, non_zero_idx[0]]


def get_columns_from_df(columns_list: np.array, df: pd.DataFrame) -> pd.DataFrame:
    """Devuelve un conjunto de columnas de un DataFrame. La utilidad de este metodo es que funciona
    para indices categoricos o strings"""
    if np.issubdtype(columns_list.dtype, np.number):
        # Obengo por indices enteros
        return get_columns_by_categorical(columns_list, df)
    # Obtengo por string/label de columna
    return df[columns_list]
