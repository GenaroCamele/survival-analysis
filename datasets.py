# En este archivo pongo funciones que obtienen y parsean datasets
import pandas as pd


def get_small_dataset():
    """Lee los DF con la informacion genica y otro con la info clinica de un dataset chico"""
    # Lee el DF con la informacion de la expresion de los genes
    gen_df = pd.read_csv('survival_propio/expression_matrix.csv', sep='\t', dtype=object)

    # Borramos los valores NaNs
    gen_df.dropna(inplace=True)

    # Hace la transpuesta para quedarnos en las filas con los pacientes y en las columnas con los genes
    gen_df = gen_df.T

    # Por cuetiones de Pandas, quedan las columnas como un rango numerico, las sacamos y
    # usamos seteamos la fila de genes como columnas
    gen_df.columns = gen_df.iloc[0]

    # Una vez seteada la fila de los genes como columnas, la borramos
    gen_df.drop('sample', inplace=True)

    # Renombramos los nombres de los indices y las columnas
    gen_df.index.name = 'PATIENT_ID'
    gen_df.columns.name = None

    # # Casteamos a flotante
    gen_df = gen_df.astype(float)

    # Devolvemos el DF de genes y el DF con los datos clinicos
    clinical_df = pd.read_csv('survival_propio/clinical_data.csv', sep='\t', index_col=0)
    clinical_df.index.name = 'PATIENT_ID'
    clinical_df.dropna(inplace=True)
    return gen_df, clinical_df


def get_big_dataset():
    """Lee los DF con la informacion genica y otro con la info clinica"""
    # Lee el DF con la informacion de la expresion de los genes
    gen_df = pd.read_csv('survival_tcga_breast/data_mRNA_median_Zscores.txt', sep='\t', dtype=object)

    # Saca la columna 'Entrez_Gene_Id' que no nos sirve
    gen_df = gen_df[gen_df.columns.difference(['Entrez_Gene_Id'])]

    # Borramos los valores NaNs
    gen_df.dropna(inplace=True)

    # Hace la transpuesta para quedarnos en las filas con los pacientes y en las columnas con los genes
    gen_df = gen_df.T

    # Por cuetiones de Pandas, quedan las columnas como un rango numerico, las sacamos y
    # usamos seteamos la fila de genes como columnas
    gen_df.columns = gen_df.iloc[0]
    # Una vez seteada la fila de los genes como columnas, la borramos
    gen_df.drop('Hugo_Symbol', inplace=True)

    # Renombramos los nombres de los indices y las columnas
    gen_df.index.name = 'PATIENT_ID'
    gen_df.columns.name = None

    # Devolvemos el DF de genes y el DF con los datos clinicos
    return gen_df, pd.read_csv('survival_tcga_breast/data_clinical_patient.txt', sep='\t', skiprows=4, index_col=0)
